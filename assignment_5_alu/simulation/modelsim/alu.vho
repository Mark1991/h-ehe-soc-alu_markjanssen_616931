-- Copyright (C) 2019  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 19.1.0 Build 670 09/22/2019 SJ Lite Edition"

-- DATE "04/11/2021 16:34:59"

-- 
-- Device: Altera 10M50DAF484C7G Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY FIFTYFIVENM;
LIBRARY IEEE;
USE FIFTYFIVENM.FIFTYFIVENM_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	hard_block IS
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic
	);
END hard_block;

-- Design Ports Information
-- ~ALTERA_TMS~	=>  Location: PIN_H2,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_TCK~	=>  Location: PIN_G2,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_TDI~	=>  Location: PIN_L4,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_TDO~	=>  Location: PIN_M5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_CONFIG_SEL~	=>  Location: PIN_H10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_nCONFIG~	=>  Location: PIN_H9,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_nSTATUS~	=>  Location: PIN_G9,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_CONF_DONE~	=>  Location: PIN_F8,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default


ARCHITECTURE structure OF hard_block IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \~ALTERA_TMS~~padout\ : std_logic;
SIGNAL \~ALTERA_TCK~~padout\ : std_logic;
SIGNAL \~ALTERA_TDI~~padout\ : std_logic;
SIGNAL \~ALTERA_CONFIG_SEL~~padout\ : std_logic;
SIGNAL \~ALTERA_nCONFIG~~padout\ : std_logic;
SIGNAL \~ALTERA_nSTATUS~~padout\ : std_logic;
SIGNAL \~ALTERA_CONF_DONE~~padout\ : std_logic;
SIGNAL \~ALTERA_TMS~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_TCK~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_TDI~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_CONFIG_SEL~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_nCONFIG~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_nSTATUS~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_CONF_DONE~~ibuf_o\ : std_logic;

BEGIN

ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
END structure;


LIBRARY ALTERA;
LIBRARY FIFTYFIVENM;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE FIFTYFIVENM.FIFTYFIVENM_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	alu IS
    PORT (
	SW : IN std_logic_vector(0 TO 9);
	KEY : IN std_logic_vector(0 TO 1);
	LEDR : OUT std_logic_vector(0 TO 9);
	HEX0 : OUT std_logic_vector(0 TO 7);
	HEX1 : OUT std_logic_vector(0 TO 7);
	HEX2 : OUT std_logic_vector(0 TO 7);
	HEX3 : OUT std_logic_vector(0 TO 7);
	HEX4 : OUT std_logic_vector(0 TO 7);
	HEX5 : OUT std_logic_vector(0 TO 7)
	);
END alu;

-- Design Ports Information
-- SW[9]	=>  Location: PIN_F15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[9]	=>  Location: PIN_B11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[8]	=>  Location: PIN_A11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[7]	=>  Location: PIN_D14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[6]	=>  Location: PIN_E14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[5]	=>  Location: PIN_C13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[4]	=>  Location: PIN_D13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[3]	=>  Location: PIN_B10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[2]	=>  Location: PIN_A10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[1]	=>  Location: PIN_A9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[0]	=>  Location: PIN_A8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[7]	=>  Location: PIN_D15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[6]	=>  Location: PIN_C17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[5]	=>  Location: PIN_D17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[4]	=>  Location: PIN_E16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[3]	=>  Location: PIN_C16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[2]	=>  Location: PIN_C15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[1]	=>  Location: PIN_E15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[0]	=>  Location: PIN_C14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[7]	=>  Location: PIN_A16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[6]	=>  Location: PIN_B17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[5]	=>  Location: PIN_A18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[4]	=>  Location: PIN_A17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[3]	=>  Location: PIN_B16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[2]	=>  Location: PIN_E18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[1]	=>  Location: PIN_D18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[0]	=>  Location: PIN_C18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[7]	=>  Location: PIN_A19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[6]	=>  Location: PIN_B22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[5]	=>  Location: PIN_C22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[4]	=>  Location: PIN_B21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[3]	=>  Location: PIN_A21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[2]	=>  Location: PIN_B19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[1]	=>  Location: PIN_A20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[0]	=>  Location: PIN_B20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[7]	=>  Location: PIN_D22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[6]	=>  Location: PIN_E17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[5]	=>  Location: PIN_D19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[4]	=>  Location: PIN_C20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[3]	=>  Location: PIN_C19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[2]	=>  Location: PIN_E21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[1]	=>  Location: PIN_E22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[0]	=>  Location: PIN_F21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[7]	=>  Location: PIN_F17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[6]	=>  Location: PIN_F20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[5]	=>  Location: PIN_F19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[4]	=>  Location: PIN_H19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[3]	=>  Location: PIN_J18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[2]	=>  Location: PIN_E19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[1]	=>  Location: PIN_E20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[0]	=>  Location: PIN_F18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[7]	=>  Location: PIN_L19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[6]	=>  Location: PIN_N20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[5]	=>  Location: PIN_N19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[4]	=>  Location: PIN_M20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[3]	=>  Location: PIN_N18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[2]	=>  Location: PIN_L18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[1]	=>  Location: PIN_K20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[0]	=>  Location: PIN_J20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[8]	=>  Location: PIN_B14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[0]	=>  Location: PIN_C10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[1]	=>  Location: PIN_C11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[2]	=>  Location: PIN_D12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[3]	=>  Location: PIN_C12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[4]	=>  Location: PIN_A12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[5]	=>  Location: PIN_B12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[6]	=>  Location: PIN_A13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[7]	=>  Location: PIN_A14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[0]	=>  Location: PIN_B8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[1]	=>  Location: PIN_A7,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF alu IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_SW : std_logic_vector(0 TO 9);
SIGNAL ww_KEY : std_logic_vector(0 TO 1);
SIGNAL ww_LEDR : std_logic_vector(0 TO 9);
SIGNAL ww_HEX0 : std_logic_vector(0 TO 7);
SIGNAL ww_HEX1 : std_logic_vector(0 TO 7);
SIGNAL ww_HEX2 : std_logic_vector(0 TO 7);
SIGNAL ww_HEX3 : std_logic_vector(0 TO 7);
SIGNAL ww_HEX4 : std_logic_vector(0 TO 7);
SIGNAL ww_HEX5 : std_logic_vector(0 TO 7);
SIGNAL \~QUARTUS_CREATED_ADC1~_CHSEL_bus\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \~QUARTUS_CREATED_ADC2~_CHSEL_bus\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \SW[9]~input_o\ : std_logic;
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \~QUARTUS_CREATED_UNVM~~busy\ : std_logic;
SIGNAL \~QUARTUS_CREATED_ADC1~~eoc\ : std_logic;
SIGNAL \~QUARTUS_CREATED_ADC2~~eoc\ : std_logic;
SIGNAL \LEDR[9]~output_o\ : std_logic;
SIGNAL \LEDR[8]~output_o\ : std_logic;
SIGNAL \LEDR[7]~output_o\ : std_logic;
SIGNAL \LEDR[6]~output_o\ : std_logic;
SIGNAL \LEDR[5]~output_o\ : std_logic;
SIGNAL \LEDR[4]~output_o\ : std_logic;
SIGNAL \LEDR[3]~output_o\ : std_logic;
SIGNAL \LEDR[2]~output_o\ : std_logic;
SIGNAL \LEDR[1]~output_o\ : std_logic;
SIGNAL \LEDR[0]~output_o\ : std_logic;
SIGNAL \HEX0[7]~output_o\ : std_logic;
SIGNAL \HEX0[6]~output_o\ : std_logic;
SIGNAL \HEX0[5]~output_o\ : std_logic;
SIGNAL \HEX0[4]~output_o\ : std_logic;
SIGNAL \HEX0[3]~output_o\ : std_logic;
SIGNAL \HEX0[2]~output_o\ : std_logic;
SIGNAL \HEX0[1]~output_o\ : std_logic;
SIGNAL \HEX0[0]~output_o\ : std_logic;
SIGNAL \HEX1[7]~output_o\ : std_logic;
SIGNAL \HEX1[6]~output_o\ : std_logic;
SIGNAL \HEX1[5]~output_o\ : std_logic;
SIGNAL \HEX1[4]~output_o\ : std_logic;
SIGNAL \HEX1[3]~output_o\ : std_logic;
SIGNAL \HEX1[2]~output_o\ : std_logic;
SIGNAL \HEX1[1]~output_o\ : std_logic;
SIGNAL \HEX1[0]~output_o\ : std_logic;
SIGNAL \HEX2[7]~output_o\ : std_logic;
SIGNAL \HEX2[6]~output_o\ : std_logic;
SIGNAL \HEX2[5]~output_o\ : std_logic;
SIGNAL \HEX2[4]~output_o\ : std_logic;
SIGNAL \HEX2[3]~output_o\ : std_logic;
SIGNAL \HEX2[2]~output_o\ : std_logic;
SIGNAL \HEX2[1]~output_o\ : std_logic;
SIGNAL \HEX2[0]~output_o\ : std_logic;
SIGNAL \HEX3[7]~output_o\ : std_logic;
SIGNAL \HEX3[6]~output_o\ : std_logic;
SIGNAL \HEX3[5]~output_o\ : std_logic;
SIGNAL \HEX3[4]~output_o\ : std_logic;
SIGNAL \HEX3[3]~output_o\ : std_logic;
SIGNAL \HEX3[2]~output_o\ : std_logic;
SIGNAL \HEX3[1]~output_o\ : std_logic;
SIGNAL \HEX3[0]~output_o\ : std_logic;
SIGNAL \HEX4[7]~output_o\ : std_logic;
SIGNAL \HEX4[6]~output_o\ : std_logic;
SIGNAL \HEX4[5]~output_o\ : std_logic;
SIGNAL \HEX4[4]~output_o\ : std_logic;
SIGNAL \HEX4[3]~output_o\ : std_logic;
SIGNAL \HEX4[2]~output_o\ : std_logic;
SIGNAL \HEX4[1]~output_o\ : std_logic;
SIGNAL \HEX4[0]~output_o\ : std_logic;
SIGNAL \HEX5[7]~output_o\ : std_logic;
SIGNAL \HEX5[6]~output_o\ : std_logic;
SIGNAL \HEX5[5]~output_o\ : std_logic;
SIGNAL \HEX5[4]~output_o\ : std_logic;
SIGNAL \HEX5[3]~output_o\ : std_logic;
SIGNAL \HEX5[2]~output_o\ : std_logic;
SIGNAL \HEX5[1]~output_o\ : std_logic;
SIGNAL \HEX5[0]~output_o\ : std_logic;
SIGNAL \SW[8]~input_o\ : std_logic;
SIGNAL \KEY[0]~input_o\ : std_logic;
SIGNAL \SW[7]~input_o\ : std_logic;
SIGNAL \SW[6]~input_o\ : std_logic;
SIGNAL \SW[5]~input_o\ : std_logic;
SIGNAL \SW[4]~input_o\ : std_logic;
SIGNAL \KEY[1]~input_o\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Mux0~1_combout\ : std_logic;
SIGNAL \SW[3]~input_o\ : std_logic;
SIGNAL \SW[2]~input_o\ : std_logic;
SIGNAL \SW[1]~input_o\ : std_logic;
SIGNAL \SW[0]~input_o\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add4~1\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add4~3\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add4~5\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add4~7\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add4~8_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add2~1\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add2~3\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add2~5\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add2~7\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add2~8_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Mux0~0_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add0~20_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add0~19_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add2~6_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add4~6_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add0~15_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add0~16_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add2~4_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add4~4_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add0~11_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add0~12_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add2~2_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add4~2_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add0~7_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add0~8_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add4~0_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add2~0_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add0~1_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add0~2_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add0~0_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add0~4_cout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add0~6\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add0~10\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add0~14\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add0~18\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add0~21_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Mux0~2_combout\ : std_logic;
SIGNAL \aluOperation|flagOperation|carryFlag~combout\ : std_logic;
SIGNAL \flagRegister|output[1]~feeder_combout\ : std_logic;
SIGNAL \aluOperation|flagOperation|comb~0_combout\ : std_logic;
SIGNAL \aluOperation|flagOperation|oVerflowFlag~0_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Mux1~0_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Mux1~1_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add0~17_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Mux1~2_combout\ : std_logic;
SIGNAL \aluOperation|flagOperation|comb~1_combout\ : std_logic;
SIGNAL \aluOperation|flagOperation|oVerflowFlag~1_combout\ : std_logic;
SIGNAL \aluOperation|flagOperation|oVerflowFlag~combout\ : std_logic;
SIGNAL \flagRegister|output[2]~feeder_combout\ : std_logic;
SIGNAL \resultRegister|output[2]~0_combout\ : std_logic;
SIGNAL \resultRegister|output[2]~2_combout\ : std_logic;
SIGNAL \aluOperation|R[2]~6_combout\ : std_logic;
SIGNAL \resultRegister|output[2]~1_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add0~13_combout\ : std_logic;
SIGNAL \aluOperation|R[2]~7_combout\ : std_logic;
SIGNAL \aluOperation|R[2]~8_combout\ : std_logic;
SIGNAL \aluOperation|R[2]~5_combout\ : std_logic;
SIGNAL \aluOperation|R[2]~9_combout\ : std_logic;
SIGNAL \aluOperation|R[1]~0_combout\ : std_logic;
SIGNAL \aluOperation|R[1]~1_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add0~9_combout\ : std_logic;
SIGNAL \aluOperation|R[1]~2_combout\ : std_logic;
SIGNAL \aluOperation|R[1]~3_combout\ : std_logic;
SIGNAL \aluOperation|R[1]~4_combout\ : std_logic;
SIGNAL \aluOperation|R[3]~12_combout\ : std_logic;
SIGNAL \aluOperation|R[3]~10_combout\ : std_logic;
SIGNAL \aluOperation|R[3]~11_combout\ : std_logic;
SIGNAL \aluOperation|R[3]~13_combout\ : std_logic;
SIGNAL \aluOperation|R[3]~14_combout\ : std_logic;
SIGNAL \aluOperation|logicUnit|Mux0~0_combout\ : std_logic;
SIGNAL \aluOperation|logicUnit|Mux0~2_combout\ : std_logic;
SIGNAL \aluOperation|logicUnit|Mux0~1_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Mux4~0_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Mux4~1_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Add0~5_combout\ : std_logic;
SIGNAL \aluOperation|logicUnit|Mux0~3_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Mux4~2_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Mux4~3_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Mux4~4_combout\ : std_logic;
SIGNAL \aluOperation|artihmeticUnit|Mux4~5_combout\ : std_logic;
SIGNAL \hexDisplay0|Mux11~0_combout\ : std_logic;
SIGNAL \hexDisplay0|Mux10~0_combout\ : std_logic;
SIGNAL \hexDisplay0|Mux9~0_combout\ : std_logic;
SIGNAL \hexDisplay0|Mux8~0_combout\ : std_logic;
SIGNAL \hexDisplay0|Mux7~0_combout\ : std_logic;
SIGNAL \hexDisplay0|Mux6~0_combout\ : std_logic;
SIGNAL \hexDisplay0|Mux5~0_combout\ : std_logic;
SIGNAL \hexDisplay2|Mux11~0_combout\ : std_logic;
SIGNAL \hexDisplay2|Mux10~0_combout\ : std_logic;
SIGNAL \hexDisplay2|Mux9~0_combout\ : std_logic;
SIGNAL \hexDisplay2|Mux8~0_combout\ : std_logic;
SIGNAL \hexDisplay2|Mux7~0_combout\ : std_logic;
SIGNAL \hexDisplay2|Mux6~0_combout\ : std_logic;
SIGNAL \hexDisplay2|Mux5~0_combout\ : std_logic;
SIGNAL \hexDisplay4|Mux11~0_combout\ : std_logic;
SIGNAL \hexDisplay4|Mux10~0_combout\ : std_logic;
SIGNAL \hexDisplay4|Mux9~0_combout\ : std_logic;
SIGNAL \hexDisplay4|Mux8~0_combout\ : std_logic;
SIGNAL \hexDisplay4|Mux7~0_combout\ : std_logic;
SIGNAL \hexDisplay4|Mux6~0_combout\ : std_logic;
SIGNAL \hexDisplay4|Mux5~0_combout\ : std_logic;
SIGNAL \functionRegister|output\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \flagRegister|output\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \resultRegister|output\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \ALT_INV_KEY[1]~input_o\ : std_logic;
SIGNAL \ALT_INV_KEY[0]~input_o\ : std_logic;
SIGNAL \ALT_INV_SW[8]~input_o\ : std_logic;
SIGNAL \hexDisplay4|ALT_INV_Mux11~0_combout\ : std_logic;
SIGNAL \hexDisplay2|ALT_INV_Mux11~0_combout\ : std_logic;
SIGNAL \hexDisplay0|ALT_INV_Mux11~0_combout\ : std_logic;

COMPONENT hard_block
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic);
END COMPONENT;

BEGIN

ww_SW <= SW;
ww_KEY <= KEY;
LEDR <= ww_LEDR;
HEX0 <= ww_HEX0;
HEX1 <= ww_HEX1;
HEX2 <= ww_HEX2;
HEX3 <= ww_HEX3;
HEX4 <= ww_HEX4;
HEX5 <= ww_HEX5;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\~QUARTUS_CREATED_ADC1~_CHSEL_bus\ <= (\~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\);

\~QUARTUS_CREATED_ADC2~_CHSEL_bus\ <= (\~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\);
\ALT_INV_KEY[1]~input_o\ <= NOT \KEY[1]~input_o\;
\ALT_INV_KEY[0]~input_o\ <= NOT \KEY[0]~input_o\;
\ALT_INV_SW[8]~input_o\ <= NOT \SW[8]~input_o\;
\hexDisplay4|ALT_INV_Mux11~0_combout\ <= NOT \hexDisplay4|Mux11~0_combout\;
\hexDisplay2|ALT_INV_Mux11~0_combout\ <= NOT \hexDisplay2|Mux11~0_combout\;
\hexDisplay0|ALT_INV_Mux11~0_combout\ <= NOT \hexDisplay0|Mux11~0_combout\;
auto_generated_inst : hard_block
PORT MAP (
	devoe => ww_devoe,
	devclrn => ww_devclrn,
	devpor => ww_devpor);

-- Location: LCCOMB_X44_Y43_N16
\~QUARTUS_CREATED_GND~I\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \~QUARTUS_CREATED_GND~I_combout\ = GND

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	combout => \~QUARTUS_CREATED_GND~I_combout\);

-- Location: IOOBUF_X49_Y54_N9
\LEDR[9]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LEDR[9]~output_o\);

-- Location: IOOBUF_X51_Y54_N9
\LEDR[8]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \SW[8]~input_o\,
	devoe => ww_devoe,
	o => \LEDR[8]~output_o\);

-- Location: IOOBUF_X56_Y54_N9
\LEDR[7]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \functionRegister|output\(3),
	devoe => ww_devoe,
	o => \LEDR[7]~output_o\);

-- Location: IOOBUF_X66_Y54_N23
\LEDR[6]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \functionRegister|output\(2),
	devoe => ww_devoe,
	o => \LEDR[6]~output_o\);

-- Location: IOOBUF_X58_Y54_N23
\LEDR[5]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \functionRegister|output\(1),
	devoe => ww_devoe,
	o => \LEDR[5]~output_o\);

-- Location: IOOBUF_X56_Y54_N30
\LEDR[4]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \functionRegister|output\(0),
	devoe => ww_devoe,
	o => \LEDR[4]~output_o\);

-- Location: IOOBUF_X46_Y54_N9
\LEDR[3]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \flagRegister|output\(0),
	devoe => ww_devoe,
	o => \LEDR[3]~output_o\);

-- Location: IOOBUF_X51_Y54_N16
\LEDR[2]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \flagRegister|output\(1),
	devoe => ww_devoe,
	o => \LEDR[2]~output_o\);

-- Location: IOOBUF_X46_Y54_N23
\LEDR[1]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \flagRegister|output\(2),
	devoe => ww_devoe,
	o => \LEDR[1]~output_o\);

-- Location: IOOBUF_X46_Y54_N2
\LEDR[0]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \LEDR[0]~output_o\);

-- Location: IOOBUF_X66_Y54_N16
\HEX0[7]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX0[7]~output_o\);

-- Location: IOOBUF_X74_Y54_N23
\HEX0[6]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexDisplay0|ALT_INV_Mux11~0_combout\,
	devoe => ww_devoe,
	o => \HEX0[6]~output_o\);

-- Location: IOOBUF_X74_Y54_N16
\HEX0[5]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexDisplay0|Mux10~0_combout\,
	devoe => ww_devoe,
	o => \HEX0[5]~output_o\);

-- Location: IOOBUF_X74_Y54_N2
\HEX0[4]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexDisplay0|Mux9~0_combout\,
	devoe => ww_devoe,
	o => \HEX0[4]~output_o\);

-- Location: IOOBUF_X62_Y54_N30
\HEX0[3]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexDisplay0|Mux8~0_combout\,
	devoe => ww_devoe,
	o => \HEX0[3]~output_o\);

-- Location: IOOBUF_X60_Y54_N2
\HEX0[2]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexDisplay0|Mux7~0_combout\,
	devoe => ww_devoe,
	o => \HEX0[2]~output_o\);

-- Location: IOOBUF_X74_Y54_N9
\HEX0[1]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexDisplay0|Mux6~0_combout\,
	devoe => ww_devoe,
	o => \HEX0[1]~output_o\);

-- Location: IOOBUF_X58_Y54_N16
\HEX0[0]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexDisplay0|Mux5~0_combout\,
	devoe => ww_devoe,
	o => \HEX0[0]~output_o\);

-- Location: IOOBUF_X60_Y54_N16
\HEX1[7]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX1[7]~output_o\);

-- Location: IOOBUF_X69_Y54_N30
\HEX1[6]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \ALT_INV_SW[8]~input_o\,
	devoe => ww_devoe,
	o => \HEX1[6]~output_o\);

-- Location: IOOBUF_X66_Y54_N30
\HEX1[5]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \ALT_INV_SW[8]~input_o\,
	devoe => ww_devoe,
	o => \HEX1[5]~output_o\);

-- Location: IOOBUF_X64_Y54_N2
\HEX1[4]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \ALT_INV_SW[8]~input_o\,
	devoe => ww_devoe,
	o => \HEX1[4]~output_o\);

-- Location: IOOBUF_X60_Y54_N9
\HEX1[3]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX1[3]~output_o\);

-- Location: IOOBUF_X78_Y49_N2
\HEX1[2]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX1[2]~output_o\);

-- Location: IOOBUF_X78_Y49_N9
\HEX1[1]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX1[1]~output_o\);

-- Location: IOOBUF_X69_Y54_N23
\HEX1[0]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX1[0]~output_o\);

-- Location: IOOBUF_X66_Y54_N9
\HEX2[7]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX2[7]~output_o\);

-- Location: IOOBUF_X78_Y43_N9
\HEX2[6]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexDisplay2|ALT_INV_Mux11~0_combout\,
	devoe => ww_devoe,
	o => \HEX2[6]~output_o\);

-- Location: IOOBUF_X78_Y35_N2
\HEX2[5]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexDisplay2|Mux10~0_combout\,
	devoe => ww_devoe,
	o => \HEX2[5]~output_o\);

-- Location: IOOBUF_X78_Y43_N2
\HEX2[4]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexDisplay2|Mux9~0_combout\,
	devoe => ww_devoe,
	o => \HEX2[4]~output_o\);

-- Location: IOOBUF_X78_Y44_N2
\HEX2[3]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexDisplay2|Mux8~0_combout\,
	devoe => ww_devoe,
	o => \HEX2[3]~output_o\);

-- Location: IOOBUF_X69_Y54_N16
\HEX2[2]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexDisplay2|Mux7~0_combout\,
	devoe => ww_devoe,
	o => \HEX2[2]~output_o\);

-- Location: IOOBUF_X66_Y54_N2
\HEX2[1]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexDisplay2|Mux6~0_combout\,
	devoe => ww_devoe,
	o => \HEX2[1]~output_o\);

-- Location: IOOBUF_X78_Y44_N9
\HEX2[0]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexDisplay2|Mux5~0_combout\,
	devoe => ww_devoe,
	o => \HEX2[0]~output_o\);

-- Location: IOOBUF_X78_Y35_N9
\HEX3[7]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX3[7]~output_o\);

-- Location: IOOBUF_X78_Y43_N16
\HEX3[6]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \ALT_INV_SW[8]~input_o\,
	devoe => ww_devoe,
	o => \HEX3[6]~output_o\);

-- Location: IOOBUF_X78_Y41_N2
\HEX3[5]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \ALT_INV_SW[8]~input_o\,
	devoe => ww_devoe,
	o => \HEX3[5]~output_o\);

-- Location: IOOBUF_X78_Y41_N9
\HEX3[4]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \ALT_INV_SW[8]~input_o\,
	devoe => ww_devoe,
	o => \HEX3[4]~output_o\);

-- Location: IOOBUF_X69_Y54_N9
\HEX3[3]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX3[3]~output_o\);

-- Location: IOOBUF_X78_Y33_N2
\HEX3[2]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX3[2]~output_o\);

-- Location: IOOBUF_X78_Y33_N9
\HEX3[1]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX3[1]~output_o\);

-- Location: IOOBUF_X78_Y35_N23
\HEX3[0]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX3[0]~output_o\);

-- Location: IOOBUF_X78_Y43_N23
\HEX4[7]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX4[7]~output_o\);

-- Location: IOOBUF_X78_Y35_N16
\HEX4[6]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexDisplay4|ALT_INV_Mux11~0_combout\,
	devoe => ww_devoe,
	o => \HEX4[6]~output_o\);

-- Location: IOOBUF_X78_Y40_N9
\HEX4[5]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexDisplay4|Mux10~0_combout\,
	devoe => ww_devoe,
	o => \HEX4[5]~output_o\);

-- Location: IOOBUF_X78_Y45_N23
\HEX4[4]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexDisplay4|Mux9~0_combout\,
	devoe => ww_devoe,
	o => \HEX4[4]~output_o\);

-- Location: IOOBUF_X78_Y42_N16
\HEX4[3]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexDisplay4|Mux8~0_combout\,
	devoe => ww_devoe,
	o => \HEX4[3]~output_o\);

-- Location: IOOBUF_X78_Y40_N23
\HEX4[2]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexDisplay4|Mux7~0_combout\,
	devoe => ww_devoe,
	o => \HEX4[2]~output_o\);

-- Location: IOOBUF_X78_Y40_N2
\HEX4[1]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexDisplay4|Mux6~0_combout\,
	devoe => ww_devoe,
	o => \HEX4[1]~output_o\);

-- Location: IOOBUF_X78_Y40_N16
\HEX4[0]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexDisplay4|Mux5~0_combout\,
	devoe => ww_devoe,
	o => \HEX4[0]~output_o\);

-- Location: IOOBUF_X78_Y37_N9
\HEX5[7]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX5[7]~output_o\);

-- Location: IOOBUF_X78_Y34_N2
\HEX5[6]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \ALT_INV_SW[8]~input_o\,
	devoe => ww_devoe,
	o => \HEX5[6]~output_o\);

-- Location: IOOBUF_X78_Y34_N16
\HEX5[5]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \ALT_INV_SW[8]~input_o\,
	devoe => ww_devoe,
	o => \HEX5[5]~output_o\);

-- Location: IOOBUF_X78_Y34_N9
\HEX5[4]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \ALT_INV_SW[8]~input_o\,
	devoe => ww_devoe,
	o => \HEX5[4]~output_o\);

-- Location: IOOBUF_X78_Y34_N24
\HEX5[3]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX5[3]~output_o\);

-- Location: IOOBUF_X78_Y37_N16
\HEX5[2]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX5[2]~output_o\);

-- Location: IOOBUF_X78_Y42_N2
\HEX5[1]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX5[1]~output_o\);

-- Location: IOOBUF_X78_Y45_N9
\HEX5[0]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX5[0]~output_o\);

-- Location: IOIBUF_X56_Y54_N1
\SW[8]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(8),
	o => \SW[8]~input_o\);

-- Location: IOIBUF_X46_Y54_N29
\KEY[0]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(0),
	o => \KEY[0]~input_o\);

-- Location: IOIBUF_X58_Y54_N29
\SW[7]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(7),
	o => \SW[7]~input_o\);

-- Location: FF_X56_Y53_N23
\functionRegister|output[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_KEY[0]~input_o\,
	asdata => \SW[7]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \functionRegister|output\(3));

-- Location: IOIBUF_X54_Y54_N15
\SW[6]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(6),
	o => \SW[6]~input_o\);

-- Location: FF_X55_Y53_N5
\functionRegister|output[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_KEY[0]~input_o\,
	asdata => \SW[6]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \functionRegister|output\(2));

-- Location: IOIBUF_X49_Y54_N1
\SW[5]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(5),
	o => \SW[5]~input_o\);

-- Location: FF_X55_Y53_N7
\functionRegister|output[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_KEY[0]~input_o\,
	asdata => \SW[5]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \functionRegister|output\(1));

-- Location: IOIBUF_X54_Y54_N22
\SW[4]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(4),
	o => \SW[4]~input_o\);

-- Location: FF_X56_Y53_N27
\functionRegister|output[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_KEY[0]~input_o\,
	asdata => \SW[4]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \functionRegister|output\(0));

-- Location: IOIBUF_X49_Y54_N29
\KEY[1]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(1),
	o => \KEY[1]~input_o\);

-- Location: LCCOMB_X55_Y53_N22
\aluOperation|artihmeticUnit|Mux0~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Mux0~1_combout\ = (\functionRegister|output\(1) & (\functionRegister|output\(2) $ (\functionRegister|output\(0)))) # (!\functionRegister|output\(1) & (!\functionRegister|output\(2) & !\functionRegister|output\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001010011001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \functionRegister|output\(1),
	datab => \functionRegister|output\(2),
	datad => \functionRegister|output\(0),
	combout => \aluOperation|artihmeticUnit|Mux0~1_combout\);

-- Location: IOIBUF_X54_Y54_N29
\SW[3]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(3),
	o => \SW[3]~input_o\);

-- Location: IOIBUF_X51_Y54_N1
\SW[2]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(2),
	o => \SW[2]~input_o\);

-- Location: IOIBUF_X51_Y54_N22
\SW[1]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(1),
	o => \SW[1]~input_o\);

-- Location: IOIBUF_X51_Y54_N29
\SW[0]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(0),
	o => \SW[0]~input_o\);

-- Location: LCCOMB_X55_Y51_N18
\aluOperation|artihmeticUnit|Add4~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Add4~0_combout\ = (\SW[4]~input_o\ & ((GND) # (!\SW[0]~input_o\))) # (!\SW[4]~input_o\ & (\SW[0]~input_o\ $ (GND)))
-- \aluOperation|artihmeticUnit|Add4~1\ = CARRY((\SW[4]~input_o\) # (!\SW[0]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011010111011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[4]~input_o\,
	datab => \SW[0]~input_o\,
	datad => VCC,
	combout => \aluOperation|artihmeticUnit|Add4~0_combout\,
	cout => \aluOperation|artihmeticUnit|Add4~1\);

-- Location: LCCOMB_X55_Y51_N20
\aluOperation|artihmeticUnit|Add4~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Add4~2_combout\ = (\SW[5]~input_o\ & ((\SW[1]~input_o\ & (!\aluOperation|artihmeticUnit|Add4~1\)) # (!\SW[1]~input_o\ & (\aluOperation|artihmeticUnit|Add4~1\ & VCC)))) # (!\SW[5]~input_o\ & ((\SW[1]~input_o\ & 
-- ((\aluOperation|artihmeticUnit|Add4~1\) # (GND))) # (!\SW[1]~input_o\ & (!\aluOperation|artihmeticUnit|Add4~1\))))
-- \aluOperation|artihmeticUnit|Add4~3\ = CARRY((\SW[5]~input_o\ & (\SW[1]~input_o\ & !\aluOperation|artihmeticUnit|Add4~1\)) # (!\SW[5]~input_o\ & ((\SW[1]~input_o\) # (!\aluOperation|artihmeticUnit|Add4~1\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100101001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \SW[5]~input_o\,
	datab => \SW[1]~input_o\,
	datad => VCC,
	cin => \aluOperation|artihmeticUnit|Add4~1\,
	combout => \aluOperation|artihmeticUnit|Add4~2_combout\,
	cout => \aluOperation|artihmeticUnit|Add4~3\);

-- Location: LCCOMB_X55_Y51_N22
\aluOperation|artihmeticUnit|Add4~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Add4~4_combout\ = ((\SW[2]~input_o\ $ (\SW[6]~input_o\ $ (\aluOperation|artihmeticUnit|Add4~3\)))) # (GND)
-- \aluOperation|artihmeticUnit|Add4~5\ = CARRY((\SW[2]~input_o\ & (\SW[6]~input_o\ & !\aluOperation|artihmeticUnit|Add4~3\)) # (!\SW[2]~input_o\ & ((\SW[6]~input_o\) # (!\aluOperation|artihmeticUnit|Add4~3\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \SW[2]~input_o\,
	datab => \SW[6]~input_o\,
	datad => VCC,
	cin => \aluOperation|artihmeticUnit|Add4~3\,
	combout => \aluOperation|artihmeticUnit|Add4~4_combout\,
	cout => \aluOperation|artihmeticUnit|Add4~5\);

-- Location: LCCOMB_X55_Y51_N24
\aluOperation|artihmeticUnit|Add4~6\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Add4~6_combout\ = (\SW[7]~input_o\ & ((\SW[3]~input_o\ & (!\aluOperation|artihmeticUnit|Add4~5\)) # (!\SW[3]~input_o\ & (\aluOperation|artihmeticUnit|Add4~5\ & VCC)))) # (!\SW[7]~input_o\ & ((\SW[3]~input_o\ & 
-- ((\aluOperation|artihmeticUnit|Add4~5\) # (GND))) # (!\SW[3]~input_o\ & (!\aluOperation|artihmeticUnit|Add4~5\))))
-- \aluOperation|artihmeticUnit|Add4~7\ = CARRY((\SW[7]~input_o\ & (\SW[3]~input_o\ & !\aluOperation|artihmeticUnit|Add4~5\)) # (!\SW[7]~input_o\ & ((\SW[3]~input_o\) # (!\aluOperation|artihmeticUnit|Add4~5\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100101001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \SW[7]~input_o\,
	datab => \SW[3]~input_o\,
	datad => VCC,
	cin => \aluOperation|artihmeticUnit|Add4~5\,
	combout => \aluOperation|artihmeticUnit|Add4~6_combout\,
	cout => \aluOperation|artihmeticUnit|Add4~7\);

-- Location: LCCOMB_X55_Y51_N26
\aluOperation|artihmeticUnit|Add4~8\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Add4~8_combout\ = \aluOperation|artihmeticUnit|Add4~7\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \aluOperation|artihmeticUnit|Add4~7\,
	combout => \aluOperation|artihmeticUnit|Add4~8_combout\);

-- Location: LCCOMB_X55_Y52_N14
\aluOperation|artihmeticUnit|Add2~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Add2~0_combout\ = (\SW[4]~input_o\ & (\SW[0]~input_o\ $ (VCC))) # (!\SW[4]~input_o\ & (\SW[0]~input_o\ & VCC))
-- \aluOperation|artihmeticUnit|Add2~1\ = CARRY((\SW[4]~input_o\ & \SW[0]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[4]~input_o\,
	datab => \SW[0]~input_o\,
	datad => VCC,
	combout => \aluOperation|artihmeticUnit|Add2~0_combout\,
	cout => \aluOperation|artihmeticUnit|Add2~1\);

-- Location: LCCOMB_X55_Y52_N16
\aluOperation|artihmeticUnit|Add2~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Add2~2_combout\ = (\SW[1]~input_o\ & ((\SW[5]~input_o\ & (\aluOperation|artihmeticUnit|Add2~1\ & VCC)) # (!\SW[5]~input_o\ & (!\aluOperation|artihmeticUnit|Add2~1\)))) # (!\SW[1]~input_o\ & ((\SW[5]~input_o\ & 
-- (!\aluOperation|artihmeticUnit|Add2~1\)) # (!\SW[5]~input_o\ & ((\aluOperation|artihmeticUnit|Add2~1\) # (GND)))))
-- \aluOperation|artihmeticUnit|Add2~3\ = CARRY((\SW[1]~input_o\ & (!\SW[5]~input_o\ & !\aluOperation|artihmeticUnit|Add2~1\)) # (!\SW[1]~input_o\ & ((!\aluOperation|artihmeticUnit|Add2~1\) # (!\SW[5]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \SW[1]~input_o\,
	datab => \SW[5]~input_o\,
	datad => VCC,
	cin => \aluOperation|artihmeticUnit|Add2~1\,
	combout => \aluOperation|artihmeticUnit|Add2~2_combout\,
	cout => \aluOperation|artihmeticUnit|Add2~3\);

-- Location: LCCOMB_X55_Y52_N18
\aluOperation|artihmeticUnit|Add2~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Add2~4_combout\ = ((\SW[2]~input_o\ $ (\SW[6]~input_o\ $ (!\aluOperation|artihmeticUnit|Add2~3\)))) # (GND)
-- \aluOperation|artihmeticUnit|Add2~5\ = CARRY((\SW[2]~input_o\ & ((\SW[6]~input_o\) # (!\aluOperation|artihmeticUnit|Add2~3\))) # (!\SW[2]~input_o\ & (\SW[6]~input_o\ & !\aluOperation|artihmeticUnit|Add2~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \SW[2]~input_o\,
	datab => \SW[6]~input_o\,
	datad => VCC,
	cin => \aluOperation|artihmeticUnit|Add2~3\,
	combout => \aluOperation|artihmeticUnit|Add2~4_combout\,
	cout => \aluOperation|artihmeticUnit|Add2~5\);

-- Location: LCCOMB_X55_Y52_N20
\aluOperation|artihmeticUnit|Add2~6\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Add2~6_combout\ = (\SW[7]~input_o\ & ((\SW[3]~input_o\ & (\aluOperation|artihmeticUnit|Add2~5\ & VCC)) # (!\SW[3]~input_o\ & (!\aluOperation|artihmeticUnit|Add2~5\)))) # (!\SW[7]~input_o\ & ((\SW[3]~input_o\ & 
-- (!\aluOperation|artihmeticUnit|Add2~5\)) # (!\SW[3]~input_o\ & ((\aluOperation|artihmeticUnit|Add2~5\) # (GND)))))
-- \aluOperation|artihmeticUnit|Add2~7\ = CARRY((\SW[7]~input_o\ & (!\SW[3]~input_o\ & !\aluOperation|artihmeticUnit|Add2~5\)) # (!\SW[7]~input_o\ & ((!\aluOperation|artihmeticUnit|Add2~5\) # (!\SW[3]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \SW[7]~input_o\,
	datab => \SW[3]~input_o\,
	datad => VCC,
	cin => \aluOperation|artihmeticUnit|Add2~5\,
	combout => \aluOperation|artihmeticUnit|Add2~6_combout\,
	cout => \aluOperation|artihmeticUnit|Add2~7\);

-- Location: LCCOMB_X55_Y52_N22
\aluOperation|artihmeticUnit|Add2~8\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Add2~8_combout\ = !\aluOperation|artihmeticUnit|Add2~7\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \aluOperation|artihmeticUnit|Add2~7\,
	combout => \aluOperation|artihmeticUnit|Add2~8_combout\);

-- Location: LCCOMB_X57_Y53_N10
\aluOperation|artihmeticUnit|Mux0~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Mux0~0_combout\ = (\functionRegister|output\(2) & (\aluOperation|artihmeticUnit|Add4~8_combout\)) # (!\functionRegister|output\(2) & (((\aluOperation|artihmeticUnit|Add2~8_combout\ & \functionRegister|output\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \aluOperation|artihmeticUnit|Add4~8_combout\,
	datab => \aluOperation|artihmeticUnit|Add2~8_combout\,
	datac => \functionRegister|output\(2),
	datad => \functionRegister|output\(1),
	combout => \aluOperation|artihmeticUnit|Mux0~0_combout\);

-- Location: LCCOMB_X57_Y53_N6
\aluOperation|artihmeticUnit|Add0~20\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Add0~20_combout\ = (\functionRegister|output\(2) & ((\functionRegister|output\(1) & (\aluOperation|artihmeticUnit|Add4~8_combout\)) # (!\functionRegister|output\(1) & ((\aluOperation|artihmeticUnit|Add2~8_combout\))))) # 
-- (!\functionRegister|output\(2) & (((\functionRegister|output\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \aluOperation|artihmeticUnit|Add4~8_combout\,
	datab => \aluOperation|artihmeticUnit|Add2~8_combout\,
	datac => \functionRegister|output\(2),
	datad => \functionRegister|output\(1),
	combout => \aluOperation|artihmeticUnit|Add0~20_combout\);

-- Location: LCCOMB_X57_Y53_N24
\aluOperation|artihmeticUnit|Add0~19\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Add0~19_combout\ = (\functionRegister|output\(0) & \functionRegister|output\(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \functionRegister|output\(0),
	datad => \functionRegister|output\(1),
	combout => \aluOperation|artihmeticUnit|Add0~19_combout\);

-- Location: LCCOMB_X56_Y53_N6
\aluOperation|artihmeticUnit|Add0~15\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Add0~15_combout\ = (\functionRegister|output\(2) & ((\functionRegister|output\(1) & ((\aluOperation|artihmeticUnit|Add4~6_combout\))) # (!\functionRegister|output\(1) & (\aluOperation|artihmeticUnit|Add2~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \aluOperation|artihmeticUnit|Add2~6_combout\,
	datab => \aluOperation|artihmeticUnit|Add4~6_combout\,
	datac => \functionRegister|output\(2),
	datad => \functionRegister|output\(1),
	combout => \aluOperation|artihmeticUnit|Add0~15_combout\);

-- Location: LCCOMB_X56_Y53_N22
\aluOperation|artihmeticUnit|Add0~16\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Add0~16_combout\ = (\aluOperation|artihmeticUnit|Add0~15_combout\) # ((!\functionRegister|output\(2) & \SW[7]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \functionRegister|output\(2),
	datac => \SW[7]~input_o\,
	datad => \aluOperation|artihmeticUnit|Add0~15_combout\,
	combout => \aluOperation|artihmeticUnit|Add0~16_combout\);

-- Location: LCCOMB_X55_Y53_N2
\aluOperation|artihmeticUnit|Add0~11\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Add0~11_combout\ = (\functionRegister|output\(2) & ((\functionRegister|output\(1) & ((\aluOperation|artihmeticUnit|Add4~4_combout\))) # (!\functionRegister|output\(1) & (\aluOperation|artihmeticUnit|Add2~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \aluOperation|artihmeticUnit|Add2~4_combout\,
	datab => \aluOperation|artihmeticUnit|Add4~4_combout\,
	datac => \functionRegister|output\(2),
	datad => \functionRegister|output\(1),
	combout => \aluOperation|artihmeticUnit|Add0~11_combout\);

-- Location: LCCOMB_X55_Y53_N0
\aluOperation|artihmeticUnit|Add0~12\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Add0~12_combout\ = (\aluOperation|artihmeticUnit|Add0~11_combout\) # ((\SW[6]~input_o\ & !\functionRegister|output\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[6]~input_o\,
	datac => \functionRegister|output\(2),
	datad => \aluOperation|artihmeticUnit|Add0~11_combout\,
	combout => \aluOperation|artihmeticUnit|Add0~12_combout\);

-- Location: LCCOMB_X55_Y53_N30
\aluOperation|artihmeticUnit|Add0~7\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Add0~7_combout\ = (\functionRegister|output\(2) & ((\functionRegister|output\(1) & ((\aluOperation|artihmeticUnit|Add4~2_combout\))) # (!\functionRegister|output\(1) & (\aluOperation|artihmeticUnit|Add2~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \aluOperation|artihmeticUnit|Add2~2_combout\,
	datab => \aluOperation|artihmeticUnit|Add4~2_combout\,
	datac => \functionRegister|output\(2),
	datad => \functionRegister|output\(1),
	combout => \aluOperation|artihmeticUnit|Add0~7_combout\);

-- Location: LCCOMB_X55_Y53_N28
\aluOperation|artihmeticUnit|Add0~8\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Add0~8_combout\ = (\aluOperation|artihmeticUnit|Add0~7_combout\) # ((\SW[5]~input_o\ & !\functionRegister|output\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001011110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[5]~input_o\,
	datab => \functionRegister|output\(2),
	datac => \aluOperation|artihmeticUnit|Add0~7_combout\,
	combout => \aluOperation|artihmeticUnit|Add0~8_combout\);

-- Location: LCCOMB_X55_Y53_N20
\aluOperation|artihmeticUnit|Add0~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Add0~1_combout\ = (\functionRegister|output\(2) & ((\functionRegister|output\(1) & (\aluOperation|artihmeticUnit|Add4~0_combout\)) # (!\functionRegister|output\(1) & ((\aluOperation|artihmeticUnit|Add2~0_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \aluOperation|artihmeticUnit|Add4~0_combout\,
	datab => \aluOperation|artihmeticUnit|Add2~0_combout\,
	datac => \functionRegister|output\(2),
	datad => \functionRegister|output\(1),
	combout => \aluOperation|artihmeticUnit|Add0~1_combout\);

-- Location: LCCOMB_X56_Y53_N26
\aluOperation|artihmeticUnit|Add0~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Add0~2_combout\ = (\aluOperation|artihmeticUnit|Add0~1_combout\) # ((!\functionRegister|output\(2) & \SW[4]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \functionRegister|output\(2),
	datac => \SW[4]~input_o\,
	datad => \aluOperation|artihmeticUnit|Add0~1_combout\,
	combout => \aluOperation|artihmeticUnit|Add0~2_combout\);

-- Location: LCCOMB_X56_Y53_N4
\aluOperation|artihmeticUnit|Add0~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Add0~0_combout\ = (\functionRegister|output\(1) $ (\flagRegister|output\(0))) # (!\functionRegister|output\(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111111110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \functionRegister|output\(2),
	datac => \functionRegister|output\(1),
	datad => \flagRegister|output\(0),
	combout => \aluOperation|artihmeticUnit|Add0~0_combout\);

-- Location: LCCOMB_X56_Y53_N10
\aluOperation|artihmeticUnit|Add0~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Add0~4_cout\ = CARRY((\functionRegister|output\(1) & \functionRegister|output\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \functionRegister|output\(1),
	datab => \functionRegister|output\(0),
	datad => VCC,
	cout => \aluOperation|artihmeticUnit|Add0~4_cout\);

-- Location: LCCOMB_X56_Y53_N12
\aluOperation|artihmeticUnit|Add0~5\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Add0~5_combout\ = (\aluOperation|artihmeticUnit|Add0~2_combout\ & ((\aluOperation|artihmeticUnit|Add0~0_combout\ & (\aluOperation|artihmeticUnit|Add0~4_cout\ & VCC)) # (!\aluOperation|artihmeticUnit|Add0~0_combout\ & 
-- (!\aluOperation|artihmeticUnit|Add0~4_cout\)))) # (!\aluOperation|artihmeticUnit|Add0~2_combout\ & ((\aluOperation|artihmeticUnit|Add0~0_combout\ & (!\aluOperation|artihmeticUnit|Add0~4_cout\)) # (!\aluOperation|artihmeticUnit|Add0~0_combout\ & 
-- ((\aluOperation|artihmeticUnit|Add0~4_cout\) # (GND)))))
-- \aluOperation|artihmeticUnit|Add0~6\ = CARRY((\aluOperation|artihmeticUnit|Add0~2_combout\ & (!\aluOperation|artihmeticUnit|Add0~0_combout\ & !\aluOperation|artihmeticUnit|Add0~4_cout\)) # (!\aluOperation|artihmeticUnit|Add0~2_combout\ & 
-- ((!\aluOperation|artihmeticUnit|Add0~4_cout\) # (!\aluOperation|artihmeticUnit|Add0~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \aluOperation|artihmeticUnit|Add0~2_combout\,
	datab => \aluOperation|artihmeticUnit|Add0~0_combout\,
	datad => VCC,
	cin => \aluOperation|artihmeticUnit|Add0~4_cout\,
	combout => \aluOperation|artihmeticUnit|Add0~5_combout\,
	cout => \aluOperation|artihmeticUnit|Add0~6\);

-- Location: LCCOMB_X56_Y53_N14
\aluOperation|artihmeticUnit|Add0~9\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Add0~9_combout\ = ((\functionRegister|output\(1) $ (\aluOperation|artihmeticUnit|Add0~8_combout\ $ (!\aluOperation|artihmeticUnit|Add0~6\)))) # (GND)
-- \aluOperation|artihmeticUnit|Add0~10\ = CARRY((\functionRegister|output\(1) & ((\aluOperation|artihmeticUnit|Add0~8_combout\) # (!\aluOperation|artihmeticUnit|Add0~6\))) # (!\functionRegister|output\(1) & (\aluOperation|artihmeticUnit|Add0~8_combout\ & 
-- !\aluOperation|artihmeticUnit|Add0~6\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \functionRegister|output\(1),
	datab => \aluOperation|artihmeticUnit|Add0~8_combout\,
	datad => VCC,
	cin => \aluOperation|artihmeticUnit|Add0~6\,
	combout => \aluOperation|artihmeticUnit|Add0~9_combout\,
	cout => \aluOperation|artihmeticUnit|Add0~10\);

-- Location: LCCOMB_X56_Y53_N16
\aluOperation|artihmeticUnit|Add0~13\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Add0~13_combout\ = (\aluOperation|artihmeticUnit|Add0~12_combout\ & ((\functionRegister|output\(1) & (\aluOperation|artihmeticUnit|Add0~10\ & VCC)) # (!\functionRegister|output\(1) & (!\aluOperation|artihmeticUnit|Add0~10\)))) 
-- # (!\aluOperation|artihmeticUnit|Add0~12_combout\ & ((\functionRegister|output\(1) & (!\aluOperation|artihmeticUnit|Add0~10\)) # (!\functionRegister|output\(1) & ((\aluOperation|artihmeticUnit|Add0~10\) # (GND)))))
-- \aluOperation|artihmeticUnit|Add0~14\ = CARRY((\aluOperation|artihmeticUnit|Add0~12_combout\ & (!\functionRegister|output\(1) & !\aluOperation|artihmeticUnit|Add0~10\)) # (!\aluOperation|artihmeticUnit|Add0~12_combout\ & 
-- ((!\aluOperation|artihmeticUnit|Add0~10\) # (!\functionRegister|output\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \aluOperation|artihmeticUnit|Add0~12_combout\,
	datab => \functionRegister|output\(1),
	datad => VCC,
	cin => \aluOperation|artihmeticUnit|Add0~10\,
	combout => \aluOperation|artihmeticUnit|Add0~13_combout\,
	cout => \aluOperation|artihmeticUnit|Add0~14\);

-- Location: LCCOMB_X56_Y53_N18
\aluOperation|artihmeticUnit|Add0~17\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Add0~17_combout\ = ((\functionRegister|output\(1) $ (\aluOperation|artihmeticUnit|Add0~16_combout\ $ (!\aluOperation|artihmeticUnit|Add0~14\)))) # (GND)
-- \aluOperation|artihmeticUnit|Add0~18\ = CARRY((\functionRegister|output\(1) & ((\aluOperation|artihmeticUnit|Add0~16_combout\) # (!\aluOperation|artihmeticUnit|Add0~14\))) # (!\functionRegister|output\(1) & (\aluOperation|artihmeticUnit|Add0~16_combout\ & 
-- !\aluOperation|artihmeticUnit|Add0~14\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \functionRegister|output\(1),
	datab => \aluOperation|artihmeticUnit|Add0~16_combout\,
	datad => VCC,
	cin => \aluOperation|artihmeticUnit|Add0~14\,
	combout => \aluOperation|artihmeticUnit|Add0~17_combout\,
	cout => \aluOperation|artihmeticUnit|Add0~18\);

-- Location: LCCOMB_X56_Y53_N20
\aluOperation|artihmeticUnit|Add0~21\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Add0~21_combout\ = \aluOperation|artihmeticUnit|Add0~20_combout\ $ (\aluOperation|artihmeticUnit|Add0~18\ $ (\aluOperation|artihmeticUnit|Add0~19_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \aluOperation|artihmeticUnit|Add0~20_combout\,
	datad => \aluOperation|artihmeticUnit|Add0~19_combout\,
	cin => \aluOperation|artihmeticUnit|Add0~18\,
	combout => \aluOperation|artihmeticUnit|Add0~21_combout\);

-- Location: LCCOMB_X56_Y53_N2
\aluOperation|artihmeticUnit|Mux0~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Mux0~2_combout\ = (\aluOperation|artihmeticUnit|Mux0~1_combout\ & (\aluOperation|artihmeticUnit|Mux0~0_combout\)) # (!\aluOperation|artihmeticUnit|Mux0~1_combout\ & ((\aluOperation|artihmeticUnit|Add0~21_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \aluOperation|artihmeticUnit|Mux0~1_combout\,
	datac => \aluOperation|artihmeticUnit|Mux0~0_combout\,
	datad => \aluOperation|artihmeticUnit|Add0~21_combout\,
	combout => \aluOperation|artihmeticUnit|Mux0~2_combout\);

-- Location: LCCOMB_X56_Y53_N0
\aluOperation|flagOperation|carryFlag\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|flagOperation|carryFlag~combout\ = (\functionRegister|output\(3) & (\aluOperation|flagOperation|carryFlag~combout\)) # (!\functionRegister|output\(3) & ((\aluOperation|artihmeticUnit|Mux0~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \aluOperation|flagOperation|carryFlag~combout\,
	datac => \functionRegister|output\(3),
	datad => \aluOperation|artihmeticUnit|Mux0~2_combout\,
	combout => \aluOperation|flagOperation|carryFlag~combout\);

-- Location: FF_X56_Y53_N29
\flagRegister|output[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_KEY[1]~input_o\,
	asdata => \aluOperation|flagOperation|carryFlag~combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \flagRegister|output\(0));

-- Location: LCCOMB_X50_Y53_N0
\flagRegister|output[1]~feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \flagRegister|output[1]~feeder_combout\ = \SW[8]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \SW[8]~input_o\,
	combout => \flagRegister|output[1]~feeder_combout\);

-- Location: FF_X50_Y53_N1
\flagRegister|output[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_KEY[1]~input_o\,
	d => \flagRegister|output[1]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \flagRegister|output\(1));

-- Location: LCCOMB_X54_Y53_N12
\aluOperation|flagOperation|comb~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|flagOperation|comb~0_combout\ = (!\functionRegister|output\(3) & \functionRegister|output\(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \functionRegister|output\(3),
	datad => \functionRegister|output\(2),
	combout => \aluOperation|flagOperation|comb~0_combout\);

-- Location: LCCOMB_X54_Y53_N2
\aluOperation|flagOperation|oVerflowFlag~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|flagOperation|oVerflowFlag~0_combout\ = \functionRegister|output\(1) $ (\SW[7]~input_o\ $ (!\SW[3]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \functionRegister|output\(1),
	datac => \SW[7]~input_o\,
	datad => \SW[3]~input_o\,
	combout => \aluOperation|flagOperation|oVerflowFlag~0_combout\);

-- Location: LCCOMB_X54_Y53_N30
\aluOperation|artihmeticUnit|Mux1~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Mux1~0_combout\ = (\functionRegister|output\(2) & ((\aluOperation|artihmeticUnit|Add4~6_combout\) # ((\functionRegister|output\(0)) # (!\functionRegister|output\(1))))) # (!\functionRegister|output\(2) & 
-- ((\functionRegister|output\(1) $ (\functionRegister|output\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \aluOperation|artihmeticUnit|Add4~6_combout\,
	datab => \functionRegister|output\(1),
	datac => \functionRegister|output\(0),
	datad => \functionRegister|output\(2),
	combout => \aluOperation|artihmeticUnit|Mux1~0_combout\);

-- Location: LCCOMB_X55_Y53_N14
\aluOperation|artihmeticUnit|Mux1~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Mux1~1_combout\ = (\functionRegister|output\(0) & (\aluOperation|artihmeticUnit|Add2~6_combout\ & (!\functionRegister|output\(2) & !\aluOperation|artihmeticUnit|Mux1~0_combout\))) # (!\functionRegister|output\(0) & 
-- (((\functionRegister|output\(2) & \aluOperation|artihmeticUnit|Mux1~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \functionRegister|output\(0),
	datab => \aluOperation|artihmeticUnit|Add2~6_combout\,
	datac => \functionRegister|output\(2),
	datad => \aluOperation|artihmeticUnit|Mux1~0_combout\,
	combout => \aluOperation|artihmeticUnit|Mux1~1_combout\);

-- Location: LCCOMB_X55_Y53_N24
\aluOperation|artihmeticUnit|Mux1~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Mux1~2_combout\ = (\aluOperation|artihmeticUnit|Mux1~1_combout\ & ((\functionRegister|output\(1)) # ((\aluOperation|artihmeticUnit|Add0~17_combout\)))) # (!\aluOperation|artihmeticUnit|Mux1~1_combout\ & 
-- (((\aluOperation|artihmeticUnit|Mux1~0_combout\ & \aluOperation|artihmeticUnit|Add0~17_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \functionRegister|output\(1),
	datab => \aluOperation|artihmeticUnit|Mux1~0_combout\,
	datac => \aluOperation|artihmeticUnit|Mux1~1_combout\,
	datad => \aluOperation|artihmeticUnit|Add0~17_combout\,
	combout => \aluOperation|artihmeticUnit|Mux1~2_combout\);

-- Location: LCCOMB_X54_Y53_N28
\aluOperation|flagOperation|comb~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|flagOperation|comb~1_combout\ = (\aluOperation|flagOperation|comb~0_combout\ & ((\SW[7]~input_o\ $ (!\aluOperation|artihmeticUnit|Mux1~2_combout\)) # (!\aluOperation|flagOperation|oVerflowFlag~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010001000101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \aluOperation|flagOperation|comb~0_combout\,
	datab => \aluOperation|flagOperation|oVerflowFlag~0_combout\,
	datac => \SW[7]~input_o\,
	datad => \aluOperation|artihmeticUnit|Mux1~2_combout\,
	combout => \aluOperation|flagOperation|comb~1_combout\);

-- Location: LCCOMB_X54_Y53_N22
\aluOperation|flagOperation|oVerflowFlag~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|flagOperation|oVerflowFlag~1_combout\ = (\aluOperation|flagOperation|comb~0_combout\ & (\aluOperation|flagOperation|oVerflowFlag~0_combout\ & (\SW[7]~input_o\ $ (\aluOperation|artihmeticUnit|Mux1~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \aluOperation|flagOperation|comb~0_combout\,
	datab => \aluOperation|flagOperation|oVerflowFlag~0_combout\,
	datac => \SW[7]~input_o\,
	datad => \aluOperation|artihmeticUnit|Mux1~2_combout\,
	combout => \aluOperation|flagOperation|oVerflowFlag~1_combout\);

-- Location: LCCOMB_X54_Y53_N16
\aluOperation|flagOperation|oVerflowFlag\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|flagOperation|oVerflowFlag~combout\ = (!\aluOperation|flagOperation|comb~1_combout\ & ((\aluOperation|flagOperation|oVerflowFlag~1_combout\) # (\aluOperation|flagOperation|oVerflowFlag~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \aluOperation|flagOperation|comb~1_combout\,
	datac => \aluOperation|flagOperation|oVerflowFlag~1_combout\,
	datad => \aluOperation|flagOperation|oVerflowFlag~combout\,
	combout => \aluOperation|flagOperation|oVerflowFlag~combout\);

-- Location: LCCOMB_X50_Y53_N6
\flagRegister|output[2]~feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \flagRegister|output[2]~feeder_combout\ = \aluOperation|flagOperation|oVerflowFlag~combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \aluOperation|flagOperation|oVerflowFlag~combout\,
	combout => \flagRegister|output[2]~feeder_combout\);

-- Location: FF_X50_Y53_N7
\flagRegister|output[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_KEY[1]~input_o\,
	d => \flagRegister|output[2]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \flagRegister|output\(2));

-- Location: LCCOMB_X54_Y53_N8
\resultRegister|output[2]~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \resultRegister|output[2]~0_combout\ = (\functionRegister|output\(1) & (!\functionRegister|output\(3) & (\functionRegister|output\(0) $ (\functionRegister|output\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \functionRegister|output\(1),
	datab => \functionRegister|output\(0),
	datac => \functionRegister|output\(3),
	datad => \functionRegister|output\(2),
	combout => \resultRegister|output[2]~0_combout\);

-- Location: LCCOMB_X55_Y53_N6
\resultRegister|output[2]~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \resultRegister|output[2]~2_combout\ = (!\functionRegister|output\(3) & ((\functionRegister|output\(2)) # (\functionRegister|output\(1) $ (\functionRegister|output\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010101010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \functionRegister|output\(3),
	datab => \functionRegister|output\(2),
	datac => \functionRegister|output\(1),
	datad => \functionRegister|output\(0),
	combout => \resultRegister|output[2]~2_combout\);

-- Location: LCCOMB_X55_Y53_N4
\aluOperation|R[2]~6\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|R[2]~6_combout\ = (\functionRegister|output\(1) & (\SW[6]~input_o\ $ (((\SW[2]~input_o\) # (\functionRegister|output\(0)))))) # (!\functionRegister|output\(1) & ((\SW[2]~input_o\ & ((\SW[6]~input_o\) # (\functionRegister|output\(0)))) # 
-- (!\SW[2]~input_o\ & (\SW[6]~input_o\ & \functionRegister|output\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111001101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \functionRegister|output\(1),
	datab => \SW[2]~input_o\,
	datac => \SW[6]~input_o\,
	datad => \functionRegister|output\(0),
	combout => \aluOperation|R[2]~6_combout\);

-- Location: LCCOMB_X54_Y53_N4
\resultRegister|output[2]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \resultRegister|output[2]~1_combout\ = (!\functionRegister|output\(3) & ((\functionRegister|output\(1) & (\functionRegister|output\(0) $ (\functionRegister|output\(2)))) # (!\functionRegister|output\(1) & (!\functionRegister|output\(0) & 
-- !\functionRegister|output\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010001000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \functionRegister|output\(3),
	datab => \functionRegister|output\(1),
	datac => \functionRegister|output\(0),
	datad => \functionRegister|output\(2),
	combout => \resultRegister|output[2]~1_combout\);

-- Location: LCCOMB_X55_Y53_N10
\aluOperation|R[2]~7\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|R[2]~7_combout\ = (\resultRegister|output[2]~2_combout\ & (((\resultRegister|output[2]~1_combout\) # (\aluOperation|artihmeticUnit|Add0~13_combout\)))) # (!\resultRegister|output[2]~2_combout\ & (\aluOperation|R[2]~6_combout\ & 
-- (!\resultRegister|output[2]~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111010100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \resultRegister|output[2]~2_combout\,
	datab => \aluOperation|R[2]~6_combout\,
	datac => \resultRegister|output[2]~1_combout\,
	datad => \aluOperation|artihmeticUnit|Add0~13_combout\,
	combout => \aluOperation|R[2]~7_combout\);

-- Location: LCCOMB_X55_Y53_N12
\aluOperation|R[2]~8\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|R[2]~8_combout\ = (\resultRegister|output[2]~0_combout\ & ((\aluOperation|R[2]~7_combout\ & (\aluOperation|artihmeticUnit|Add4~4_combout\)) # (!\aluOperation|R[2]~7_combout\ & ((\aluOperation|artihmeticUnit|Add2~4_combout\))))) # 
-- (!\resultRegister|output[2]~0_combout\ & (\aluOperation|R[2]~7_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \resultRegister|output[2]~0_combout\,
	datab => \aluOperation|R[2]~7_combout\,
	datac => \aluOperation|artihmeticUnit|Add4~4_combout\,
	datad => \aluOperation|artihmeticUnit|Add2~4_combout\,
	combout => \aluOperation|R[2]~8_combout\);

-- Location: LCCOMB_X55_Y52_N2
\aluOperation|R[2]~5\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|R[2]~5_combout\ = (\functionRegister|output\(1) & (\SW[7]~input_o\)) # (!\functionRegister|output\(1) & ((\SW[5]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110010101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[7]~input_o\,
	datab => \SW[5]~input_o\,
	datac => \functionRegister|output\(1),
	combout => \aluOperation|R[2]~5_combout\);

-- Location: LCCOMB_X55_Y53_N26
\aluOperation|R[2]~9\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|R[2]~9_combout\ = (\functionRegister|output\(3) & ((\functionRegister|output\(2) & ((\aluOperation|R[2]~5_combout\))) # (!\functionRegister|output\(2) & (\aluOperation|R[2]~8_combout\)))) # (!\functionRegister|output\(3) & 
-- (\aluOperation|R[2]~8_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \aluOperation|R[2]~8_combout\,
	datab => \aluOperation|R[2]~5_combout\,
	datac => \functionRegister|output\(3),
	datad => \functionRegister|output\(2),
	combout => \aluOperation|R[2]~9_combout\);

-- Location: FF_X55_Y53_N27
\resultRegister|output[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_KEY[1]~input_o\,
	d => \aluOperation|R[2]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \resultRegister|output\(2));

-- Location: LCCOMB_X54_Y53_N10
\aluOperation|R[1]~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|R[1]~0_combout\ = (\functionRegister|output\(1) & (\SW[6]~input_o\)) # (!\functionRegister|output\(1) & ((\SW[4]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[6]~input_o\,
	datac => \functionRegister|output\(1),
	datad => \SW[4]~input_o\,
	combout => \aluOperation|R[1]~0_combout\);

-- Location: LCCOMB_X54_Y53_N18
\aluOperation|R[1]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|R[1]~1_combout\ = (\SW[5]~input_o\ & (\functionRegister|output\(1) $ (((\SW[1]~input_o\) # (\functionRegister|output\(0)))))) # (!\SW[5]~input_o\ & ((\SW[1]~input_o\ & ((\functionRegister|output\(0)) # (\functionRegister|output\(1)))) # 
-- (!\SW[1]~input_o\ & (\functionRegister|output\(0) & \functionRegister|output\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101011011101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[5]~input_o\,
	datab => \SW[1]~input_o\,
	datac => \functionRegister|output\(0),
	datad => \functionRegister|output\(1),
	combout => \aluOperation|R[1]~1_combout\);

-- Location: LCCOMB_X55_Y53_N18
\aluOperation|R[1]~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|R[1]~2_combout\ = (\resultRegister|output[2]~2_combout\ & (((\aluOperation|artihmeticUnit|Add0~9_combout\) # (\resultRegister|output[2]~1_combout\)))) # (!\resultRegister|output[2]~2_combout\ & (\aluOperation|R[1]~1_combout\ & 
-- ((!\resultRegister|output[2]~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \resultRegister|output[2]~2_combout\,
	datab => \aluOperation|R[1]~1_combout\,
	datac => \aluOperation|artihmeticUnit|Add0~9_combout\,
	datad => \resultRegister|output[2]~1_combout\,
	combout => \aluOperation|R[1]~2_combout\);

-- Location: LCCOMB_X55_Y53_N8
\aluOperation|R[1]~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|R[1]~3_combout\ = (\resultRegister|output[2]~0_combout\ & ((\aluOperation|R[1]~2_combout\ & (\aluOperation|artihmeticUnit|Add4~2_combout\)) # (!\aluOperation|R[1]~2_combout\ & ((\aluOperation|artihmeticUnit|Add2~2_combout\))))) # 
-- (!\resultRegister|output[2]~0_combout\ & (((\aluOperation|R[1]~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101011010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \resultRegister|output[2]~0_combout\,
	datab => \aluOperation|artihmeticUnit|Add4~2_combout\,
	datac => \aluOperation|R[1]~2_combout\,
	datad => \aluOperation|artihmeticUnit|Add2~2_combout\,
	combout => \aluOperation|R[1]~3_combout\);

-- Location: LCCOMB_X55_Y53_N16
\aluOperation|R[1]~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|R[1]~4_combout\ = (\functionRegister|output\(3) & ((\functionRegister|output\(2) & (\aluOperation|R[1]~0_combout\)) # (!\functionRegister|output\(2) & ((\aluOperation|R[1]~3_combout\))))) # (!\functionRegister|output\(3) & 
-- (((\aluOperation|R[1]~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \aluOperation|R[1]~0_combout\,
	datab => \aluOperation|R[1]~3_combout\,
	datac => \functionRegister|output\(3),
	datad => \functionRegister|output\(2),
	combout => \aluOperation|R[1]~4_combout\);

-- Location: FF_X55_Y53_N17
\resultRegister|output[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_KEY[1]~input_o\,
	d => \aluOperation|R[1]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \resultRegister|output\(1));

-- Location: LCCOMB_X54_Y53_N20
\aluOperation|R[3]~12\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|R[3]~12_combout\ = (!\functionRegister|output\(0) & \functionRegister|output\(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \functionRegister|output\(0),
	datad => \functionRegister|output\(1),
	combout => \aluOperation|R[3]~12_combout\);

-- Location: LCCOMB_X54_Y53_N0
\aluOperation|R[3]~10\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|R[3]~10_combout\ = (\functionRegister|output\(1) & ((\SW[4]~input_o\))) # (!\functionRegister|output\(1) & (\SW[6]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[6]~input_o\,
	datac => \functionRegister|output\(1),
	datad => \SW[4]~input_o\,
	combout => \aluOperation|R[3]~10_combout\);

-- Location: LCCOMB_X54_Y53_N6
\aluOperation|R[3]~11\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|R[3]~11_combout\ = (\functionRegister|output\(1) & (\SW[7]~input_o\ $ (((\functionRegister|output\(0)) # (\SW[3]~input_o\))))) # (!\functionRegister|output\(1) & ((\functionRegister|output\(0) & ((\SW[7]~input_o\) # (\SW[3]~input_o\))) # 
-- (!\functionRegister|output\(0) & (\SW[7]~input_o\ & \SW[3]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111001101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \functionRegister|output\(1),
	datab => \functionRegister|output\(0),
	datac => \SW[7]~input_o\,
	datad => \SW[3]~input_o\,
	combout => \aluOperation|R[3]~11_combout\);

-- Location: LCCOMB_X54_Y53_N26
\aluOperation|R[3]~13\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|R[3]~13_combout\ = (\functionRegister|output\(2) & (!\aluOperation|R[3]~12_combout\ & (\aluOperation|R[3]~10_combout\))) # (!\functionRegister|output\(2) & (((\aluOperation|R[3]~11_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \functionRegister|output\(2),
	datab => \aluOperation|R[3]~12_combout\,
	datac => \aluOperation|R[3]~10_combout\,
	datad => \aluOperation|R[3]~11_combout\,
	combout => \aluOperation|R[3]~13_combout\);

-- Location: LCCOMB_X54_Y53_N24
\aluOperation|R[3]~14\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|R[3]~14_combout\ = (\functionRegister|output\(3) & (\aluOperation|R[3]~13_combout\)) # (!\functionRegister|output\(3) & ((\aluOperation|artihmeticUnit|Mux1~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \aluOperation|R[3]~13_combout\,
	datac => \functionRegister|output\(3),
	datad => \aluOperation|artihmeticUnit|Mux1~2_combout\,
	combout => \aluOperation|R[3]~14_combout\);

-- Location: FF_X54_Y53_N25
\resultRegister|output[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_KEY[1]~input_o\,
	d => \aluOperation|R[3]~14_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \resultRegister|output\(3));

-- Location: LCCOMB_X55_Y52_N8
\aluOperation|logicUnit|Mux0~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|logicUnit|Mux0~0_combout\ = (\SW[7]~input_o\ & \functionRegister|output\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[7]~input_o\,
	datac => \functionRegister|output\(0),
	combout => \aluOperation|logicUnit|Mux0~0_combout\);

-- Location: LCCOMB_X55_Y52_N24
\aluOperation|logicUnit|Mux0~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|logicUnit|Mux0~2_combout\ = (\SW[4]~input_o\ & ((\functionRegister|output\(0)) # (\SW[0]~input_o\))) # (!\SW[4]~input_o\ & (\functionRegister|output\(0) & \SW[0]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[4]~input_o\,
	datac => \functionRegister|output\(0),
	datad => \SW[0]~input_o\,
	combout => \aluOperation|logicUnit|Mux0~2_combout\);

-- Location: LCCOMB_X55_Y52_N10
\aluOperation|logicUnit|Mux0~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|logicUnit|Mux0~1_combout\ = (\functionRegister|output\(0) & (!\SW[4]~input_o\)) # (!\functionRegister|output\(0) & ((\aluOperation|artihmeticUnit|Add2~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101110001011100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[4]~input_o\,
	datab => \aluOperation|artihmeticUnit|Add2~0_combout\,
	datac => \functionRegister|output\(0),
	combout => \aluOperation|logicUnit|Mux0~1_combout\);

-- Location: LCCOMB_X55_Y52_N6
\aluOperation|artihmeticUnit|Mux4~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Mux4~0_combout\ = (\functionRegister|output\(1) & (((\functionRegister|output\(2)) # (\aluOperation|logicUnit|Mux0~1_combout\)))) # (!\functionRegister|output\(1) & (\aluOperation|logicUnit|Mux0~2_combout\ & 
-- (!\functionRegister|output\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111010100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \functionRegister|output\(1),
	datab => \aluOperation|logicUnit|Mux0~2_combout\,
	datac => \functionRegister|output\(2),
	datad => \aluOperation|logicUnit|Mux0~1_combout\,
	combout => \aluOperation|artihmeticUnit|Mux4~0_combout\);

-- Location: LCCOMB_X55_Y52_N28
\aluOperation|artihmeticUnit|Mux4~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Mux4~1_combout\ = (\functionRegister|output\(2) & ((\aluOperation|artihmeticUnit|Mux4~0_combout\ & (\SW[5]~input_o\)) # (!\aluOperation|artihmeticUnit|Mux4~0_combout\ & ((\aluOperation|logicUnit|Mux0~0_combout\))))) # 
-- (!\functionRegister|output\(2) & (((\aluOperation|artihmeticUnit|Mux4~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \functionRegister|output\(2),
	datab => \SW[5]~input_o\,
	datac => \aluOperation|logicUnit|Mux0~0_combout\,
	datad => \aluOperation|artihmeticUnit|Mux4~0_combout\,
	combout => \aluOperation|artihmeticUnit|Mux4~1_combout\);

-- Location: LCCOMB_X56_Y53_N28
\aluOperation|logicUnit|Mux0~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|logicUnit|Mux0~3_combout\ = (\functionRegister|output\(0) & ((\aluOperation|artihmeticUnit|Add0~5_combout\))) # (!\functionRegister|output\(0) & (\aluOperation|artihmeticUnit|Add4~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \aluOperation|artihmeticUnit|Add4~0_combout\,
	datab => \functionRegister|output\(0),
	datad => \aluOperation|artihmeticUnit|Add0~5_combout\,
	combout => \aluOperation|logicUnit|Mux0~3_combout\);

-- Location: LCCOMB_X56_Y53_N30
\aluOperation|artihmeticUnit|Mux4~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Mux4~2_combout\ = (\functionRegister|output\(1) & ((\functionRegister|output\(0) & (\aluOperation|artihmeticUnit|Add2~0_combout\)) # (!\functionRegister|output\(0) & ((\aluOperation|artihmeticUnit|Add0~5_combout\))))) # 
-- (!\functionRegister|output\(1) & (((\functionRegister|output\(0) & \aluOperation|artihmeticUnit|Add0~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \aluOperation|artihmeticUnit|Add2~0_combout\,
	datab => \functionRegister|output\(1),
	datac => \functionRegister|output\(0),
	datad => \aluOperation|artihmeticUnit|Add0~5_combout\,
	combout => \aluOperation|artihmeticUnit|Mux4~2_combout\);

-- Location: LCCOMB_X57_Y53_N4
\aluOperation|artihmeticUnit|Mux4~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Mux4~3_combout\ = (\functionRegister|output\(2) & (\functionRegister|output\(1))) # (!\functionRegister|output\(2) & ((\aluOperation|artihmeticUnit|Mux4~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \functionRegister|output\(1),
	datac => \functionRegister|output\(2),
	datad => \aluOperation|artihmeticUnit|Mux4~2_combout\,
	combout => \aluOperation|artihmeticUnit|Mux4~3_combout\);

-- Location: LCCOMB_X56_Y53_N24
\aluOperation|artihmeticUnit|Mux4~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Mux4~4_combout\ = (\functionRegister|output\(2) & ((\aluOperation|artihmeticUnit|Mux4~3_combout\ & ((\aluOperation|logicUnit|Mux0~3_combout\))) # (!\aluOperation|artihmeticUnit|Mux4~3_combout\ & 
-- (\aluOperation|artihmeticUnit|Add0~5_combout\)))) # (!\functionRegister|output\(2) & (((\aluOperation|artihmeticUnit|Mux4~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \aluOperation|artihmeticUnit|Add0~5_combout\,
	datab => \functionRegister|output\(2),
	datac => \aluOperation|logicUnit|Mux0~3_combout\,
	datad => \aluOperation|artihmeticUnit|Mux4~3_combout\,
	combout => \aluOperation|artihmeticUnit|Mux4~4_combout\);

-- Location: LCCOMB_X56_Y53_N8
\aluOperation|artihmeticUnit|Mux4~5\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \aluOperation|artihmeticUnit|Mux4~5_combout\ = (\functionRegister|output\(3) & (\aluOperation|artihmeticUnit|Mux4~1_combout\)) # (!\functionRegister|output\(3) & ((\aluOperation|artihmeticUnit|Mux4~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \aluOperation|artihmeticUnit|Mux4~1_combout\,
	datac => \functionRegister|output\(3),
	datad => \aluOperation|artihmeticUnit|Mux4~4_combout\,
	combout => \aluOperation|artihmeticUnit|Mux4~5_combout\);

-- Location: FF_X56_Y53_N9
\resultRegister|output[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_KEY[1]~input_o\,
	d => \aluOperation|artihmeticUnit|Mux4~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \resultRegister|output\(0));

-- Location: LCCOMB_X62_Y53_N24
\hexDisplay0|Mux11~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \hexDisplay0|Mux11~0_combout\ = (\resultRegister|output\(0) & ((\resultRegister|output\(3)) # (\resultRegister|output\(2) $ (\resultRegister|output\(1))))) # (!\resultRegister|output\(0) & ((\resultRegister|output\(1)) # (\resultRegister|output\(2) $ 
-- (\resultRegister|output\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111011011011110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \resultRegister|output\(2),
	datab => \resultRegister|output\(1),
	datac => \resultRegister|output\(3),
	datad => \resultRegister|output\(0),
	combout => \hexDisplay0|Mux11~0_combout\);

-- Location: LCCOMB_X62_Y53_N26
\hexDisplay0|Mux10~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \hexDisplay0|Mux10~0_combout\ = (\resultRegister|output\(2) & (\resultRegister|output\(0) & (\resultRegister|output\(1) $ (\resultRegister|output\(3))))) # (!\resultRegister|output\(2) & (!\resultRegister|output\(3) & ((\resultRegister|output\(1)) # 
-- (\resultRegister|output\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010110100000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \resultRegister|output\(2),
	datab => \resultRegister|output\(1),
	datac => \resultRegister|output\(3),
	datad => \resultRegister|output\(0),
	combout => \hexDisplay0|Mux10~0_combout\);

-- Location: LCCOMB_X62_Y53_N16
\hexDisplay0|Mux9~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \hexDisplay0|Mux9~0_combout\ = (\resultRegister|output\(1) & (((!\resultRegister|output\(3) & \resultRegister|output\(0))))) # (!\resultRegister|output\(1) & ((\resultRegister|output\(2) & (!\resultRegister|output\(3))) # (!\resultRegister|output\(2) & 
-- ((\resultRegister|output\(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111100000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \resultRegister|output\(2),
	datab => \resultRegister|output\(1),
	datac => \resultRegister|output\(3),
	datad => \resultRegister|output\(0),
	combout => \hexDisplay0|Mux9~0_combout\);

-- Location: LCCOMB_X62_Y53_N2
\hexDisplay0|Mux8~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \hexDisplay0|Mux8~0_combout\ = (\resultRegister|output\(1) & ((\resultRegister|output\(2) & ((\resultRegister|output\(0)))) # (!\resultRegister|output\(2) & (\resultRegister|output\(3) & !\resultRegister|output\(0))))) # (!\resultRegister|output\(1) & 
-- (!\resultRegister|output\(3) & (\resultRegister|output\(2) $ (\resultRegister|output\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100101000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \resultRegister|output\(2),
	datab => \resultRegister|output\(1),
	datac => \resultRegister|output\(3),
	datad => \resultRegister|output\(0),
	combout => \hexDisplay0|Mux8~0_combout\);

-- Location: LCCOMB_X62_Y53_N8
\hexDisplay0|Mux7~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \hexDisplay0|Mux7~0_combout\ = (\resultRegister|output\(2) & (\resultRegister|output\(3) & ((\resultRegister|output\(1)) # (!\resultRegister|output\(0))))) # (!\resultRegister|output\(2) & (\resultRegister|output\(1) & (!\resultRegister|output\(3) & 
-- !\resultRegister|output\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000010100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \resultRegister|output\(2),
	datab => \resultRegister|output\(1),
	datac => \resultRegister|output\(3),
	datad => \resultRegister|output\(0),
	combout => \hexDisplay0|Mux7~0_combout\);

-- Location: LCCOMB_X62_Y53_N22
\hexDisplay0|Mux6~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \hexDisplay0|Mux6~0_combout\ = (\resultRegister|output\(1) & ((\resultRegister|output\(0) & ((\resultRegister|output\(3)))) # (!\resultRegister|output\(0) & (\resultRegister|output\(2))))) # (!\resultRegister|output\(1) & (\resultRegister|output\(2) & 
-- (\resultRegister|output\(3) $ (\resultRegister|output\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \resultRegister|output\(2),
	datab => \resultRegister|output\(1),
	datac => \resultRegister|output\(3),
	datad => \resultRegister|output\(0),
	combout => \hexDisplay0|Mux6~0_combout\);

-- Location: LCCOMB_X62_Y53_N12
\hexDisplay0|Mux5~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \hexDisplay0|Mux5~0_combout\ = (\resultRegister|output\(2) & (!\resultRegister|output\(1) & (\resultRegister|output\(3) $ (!\resultRegister|output\(0))))) # (!\resultRegister|output\(2) & (\resultRegister|output\(0) & (\resultRegister|output\(1) $ 
-- (!\resultRegister|output\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110000100000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \resultRegister|output\(2),
	datab => \resultRegister|output\(1),
	datac => \resultRegister|output\(3),
	datad => \resultRegister|output\(0),
	combout => \hexDisplay0|Mux5~0_combout\);

-- Location: LCCOMB_X55_Y51_N8
\hexDisplay2|Mux11~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \hexDisplay2|Mux11~0_combout\ = (\SW[0]~input_o\ & ((\SW[3]~input_o\) # (\SW[1]~input_o\ $ (\SW[2]~input_o\)))) # (!\SW[0]~input_o\ & ((\SW[1]~input_o\) # (\SW[2]~input_o\ $ (\SW[3]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111101111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[0]~input_o\,
	datab => \SW[1]~input_o\,
	datac => \SW[2]~input_o\,
	datad => \SW[3]~input_o\,
	combout => \hexDisplay2|Mux11~0_combout\);

-- Location: LCCOMB_X55_Y51_N6
\hexDisplay2|Mux10~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \hexDisplay2|Mux10~0_combout\ = (\SW[0]~input_o\ & (\SW[3]~input_o\ $ (((\SW[1]~input_o\) # (!\SW[2]~input_o\))))) # (!\SW[0]~input_o\ & (\SW[1]~input_o\ & (!\SW[2]~input_o\ & !\SW[3]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000010001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[0]~input_o\,
	datab => \SW[1]~input_o\,
	datac => \SW[2]~input_o\,
	datad => \SW[3]~input_o\,
	combout => \hexDisplay2|Mux10~0_combout\);

-- Location: LCCOMB_X54_Y51_N24
\hexDisplay2|Mux9~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \hexDisplay2|Mux9~0_combout\ = (\SW[1]~input_o\ & (\SW[0]~input_o\ & ((!\SW[3]~input_o\)))) # (!\SW[1]~input_o\ & ((\SW[2]~input_o\ & ((!\SW[3]~input_o\))) # (!\SW[2]~input_o\ & (\SW[0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001010111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[0]~input_o\,
	datab => \SW[1]~input_o\,
	datac => \SW[2]~input_o\,
	datad => \SW[3]~input_o\,
	combout => \hexDisplay2|Mux9~0_combout\);

-- Location: LCCOMB_X54_Y51_N2
\hexDisplay2|Mux8~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \hexDisplay2|Mux8~0_combout\ = (\SW[1]~input_o\ & ((\SW[0]~input_o\ & (\SW[2]~input_o\)) # (!\SW[0]~input_o\ & (!\SW[2]~input_o\ & \SW[3]~input_o\)))) # (!\SW[1]~input_o\ & (!\SW[3]~input_o\ & (\SW[0]~input_o\ $ (\SW[2]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010010010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[0]~input_o\,
	datab => \SW[1]~input_o\,
	datac => \SW[2]~input_o\,
	datad => \SW[3]~input_o\,
	combout => \hexDisplay2|Mux8~0_combout\);

-- Location: LCCOMB_X55_Y51_N4
\hexDisplay2|Mux7~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \hexDisplay2|Mux7~0_combout\ = (\SW[2]~input_o\ & (\SW[3]~input_o\ & ((\SW[1]~input_o\) # (!\SW[0]~input_o\)))) # (!\SW[2]~input_o\ & (!\SW[0]~input_o\ & (\SW[1]~input_o\ & !\SW[3]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[0]~input_o\,
	datab => \SW[1]~input_o\,
	datac => \SW[2]~input_o\,
	datad => \SW[3]~input_o\,
	combout => \hexDisplay2|Mux7~0_combout\);

-- Location: LCCOMB_X55_Y51_N14
\hexDisplay2|Mux6~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \hexDisplay2|Mux6~0_combout\ = (\SW[1]~input_o\ & ((\SW[0]~input_o\ & ((\SW[3]~input_o\))) # (!\SW[0]~input_o\ & (\SW[2]~input_o\)))) # (!\SW[1]~input_o\ & (\SW[2]~input_o\ & (\SW[0]~input_o\ $ (\SW[3]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100001100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[0]~input_o\,
	datab => \SW[1]~input_o\,
	datac => \SW[2]~input_o\,
	datad => \SW[3]~input_o\,
	combout => \hexDisplay2|Mux6~0_combout\);

-- Location: LCCOMB_X54_Y51_N16
\hexDisplay2|Mux5~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \hexDisplay2|Mux5~0_combout\ = (\SW[2]~input_o\ & (!\SW[1]~input_o\ & (\SW[0]~input_o\ $ (!\SW[3]~input_o\)))) # (!\SW[2]~input_o\ & (\SW[0]~input_o\ & (\SW[1]~input_o\ $ (!\SW[3]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100000010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[0]~input_o\,
	datab => \SW[1]~input_o\,
	datac => \SW[2]~input_o\,
	datad => \SW[3]~input_o\,
	combout => \hexDisplay2|Mux5~0_combout\);

-- Location: LCCOMB_X55_Y51_N16
\hexDisplay4|Mux11~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \hexDisplay4|Mux11~0_combout\ = (\SW[4]~input_o\ & ((\SW[7]~input_o\) # (\SW[6]~input_o\ $ (\SW[5]~input_o\)))) # (!\SW[4]~input_o\ & ((\SW[5]~input_o\) # (\SW[6]~input_o\ $ (\SW[7]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101101111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[4]~input_o\,
	datab => \SW[6]~input_o\,
	datac => \SW[5]~input_o\,
	datad => \SW[7]~input_o\,
	combout => \hexDisplay4|Mux11~0_combout\);

-- Location: LCCOMB_X55_Y51_N2
\hexDisplay4|Mux10~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \hexDisplay4|Mux10~0_combout\ = (\SW[4]~input_o\ & (\SW[7]~input_o\ $ (((\SW[5]~input_o\) # (!\SW[6]~input_o\))))) # (!\SW[4]~input_o\ & (!\SW[6]~input_o\ & (\SW[5]~input_o\ & !\SW[7]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100010110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[4]~input_o\,
	datab => \SW[6]~input_o\,
	datac => \SW[5]~input_o\,
	datad => \SW[7]~input_o\,
	combout => \hexDisplay4|Mux10~0_combout\);

-- Location: LCCOMB_X55_Y51_N28
\hexDisplay4|Mux9~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \hexDisplay4|Mux9~0_combout\ = (\SW[5]~input_o\ & (\SW[4]~input_o\ & ((!\SW[7]~input_o\)))) # (!\SW[5]~input_o\ & ((\SW[6]~input_o\ & ((!\SW[7]~input_o\))) # (!\SW[6]~input_o\ & (\SW[4]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001010101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[4]~input_o\,
	datab => \SW[6]~input_o\,
	datac => \SW[5]~input_o\,
	datad => \SW[7]~input_o\,
	combout => \hexDisplay4|Mux9~0_combout\);

-- Location: LCCOMB_X55_Y51_N30
\hexDisplay4|Mux8~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \hexDisplay4|Mux8~0_combout\ = (\SW[5]~input_o\ & ((\SW[4]~input_o\ & (\SW[6]~input_o\)) # (!\SW[4]~input_o\ & (!\SW[6]~input_o\ & \SW[7]~input_o\)))) # (!\SW[5]~input_o\ & (!\SW[7]~input_o\ & (\SW[4]~input_o\ $ (\SW[6]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001000010000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[4]~input_o\,
	datab => \SW[6]~input_o\,
	datac => \SW[5]~input_o\,
	datad => \SW[7]~input_o\,
	combout => \hexDisplay4|Mux8~0_combout\);

-- Location: LCCOMB_X55_Y51_N0
\hexDisplay4|Mux7~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \hexDisplay4|Mux7~0_combout\ = (\SW[6]~input_o\ & (\SW[7]~input_o\ & ((\SW[5]~input_o\) # (!\SW[4]~input_o\)))) # (!\SW[6]~input_o\ & (!\SW[4]~input_o\ & (\SW[5]~input_o\ & !\SW[7]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100010000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[4]~input_o\,
	datab => \SW[6]~input_o\,
	datac => \SW[5]~input_o\,
	datad => \SW[7]~input_o\,
	combout => \hexDisplay4|Mux7~0_combout\);

-- Location: LCCOMB_X55_Y51_N12
\hexDisplay4|Mux6~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \hexDisplay4|Mux6~0_combout\ = (\SW[5]~input_o\ & ((\SW[4]~input_o\ & ((\SW[7]~input_o\))) # (!\SW[4]~input_o\ & (\SW[6]~input_o\)))) # (!\SW[5]~input_o\ & (\SW[6]~input_o\ & (\SW[4]~input_o\ $ (\SW[7]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010001001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[4]~input_o\,
	datab => \SW[6]~input_o\,
	datac => \SW[5]~input_o\,
	datad => \SW[7]~input_o\,
	combout => \hexDisplay4|Mux6~0_combout\);

-- Location: LCCOMB_X55_Y51_N10
\hexDisplay4|Mux5~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \hexDisplay4|Mux5~0_combout\ = (\SW[6]~input_o\ & (!\SW[5]~input_o\ & (\SW[4]~input_o\ $ (!\SW[7]~input_o\)))) # (!\SW[6]~input_o\ & (\SW[4]~input_o\ & (\SW[5]~input_o\ $ (!\SW[7]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100000000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[4]~input_o\,
	datab => \SW[6]~input_o\,
	datac => \SW[5]~input_o\,
	datad => \SW[7]~input_o\,
	combout => \hexDisplay4|Mux5~0_combout\);

-- Location: IOIBUF_X69_Y54_N1
\SW[9]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(9),
	o => \SW[9]~input_o\);

-- Location: UNVM_X0_Y40_N40
\~QUARTUS_CREATED_UNVM~\ : fiftyfivenm_unvm
-- pragma translate_off
GENERIC MAP (
	addr_range1_end_addr => -1,
	addr_range1_offset => -1,
	addr_range2_end_addr => -1,
	addr_range2_offset => -1,
	addr_range3_offset => -1,
	is_compressed_image => "false",
	is_dual_boot => "false",
	is_eram_skip => "false",
	max_ufm_valid_addr => -1,
	max_valid_addr => -1,
	min_ufm_valid_addr => -1,
	min_valid_addr => -1,
	part_name => "quartus_created_unvm",
	reserve_block => "true")
-- pragma translate_on
PORT MAP (
	nosc_ena => \~QUARTUS_CREATED_GND~I_combout\,
	xe_ye => \~QUARTUS_CREATED_GND~I_combout\,
	se => \~QUARTUS_CREATED_GND~I_combout\,
	busy => \~QUARTUS_CREATED_UNVM~~busy\);

-- Location: ADCBLOCK_X43_Y52_N0
\~QUARTUS_CREATED_ADC1~\ : fiftyfivenm_adcblock
-- pragma translate_off
GENERIC MAP (
	analog_input_pin_mask => 0,
	clkdiv => 1,
	device_partname_fivechar_prefix => "none",
	is_this_first_or_second_adc => 1,
	prescalar => 0,
	pwd => 1,
	refsel => 0,
	reserve_block => "true",
	testbits => 66,
	tsclkdiv => 1,
	tsclksel => 0)
-- pragma translate_on
PORT MAP (
	soc => \~QUARTUS_CREATED_GND~I_combout\,
	usr_pwd => VCC,
	tsen => \~QUARTUS_CREATED_GND~I_combout\,
	chsel => \~QUARTUS_CREATED_ADC1~_CHSEL_bus\,
	eoc => \~QUARTUS_CREATED_ADC1~~eoc\);

-- Location: ADCBLOCK_X43_Y51_N0
\~QUARTUS_CREATED_ADC2~\ : fiftyfivenm_adcblock
-- pragma translate_off
GENERIC MAP (
	analog_input_pin_mask => 0,
	clkdiv => 1,
	device_partname_fivechar_prefix => "none",
	is_this_first_or_second_adc => 2,
	prescalar => 0,
	pwd => 1,
	refsel => 0,
	reserve_block => "true",
	testbits => 66,
	tsclkdiv => 1,
	tsclksel => 0)
-- pragma translate_on
PORT MAP (
	soc => \~QUARTUS_CREATED_GND~I_combout\,
	usr_pwd => VCC,
	tsen => \~QUARTUS_CREATED_GND~I_combout\,
	chsel => \~QUARTUS_CREATED_ADC2~_CHSEL_bus\,
	eoc => \~QUARTUS_CREATED_ADC2~~eoc\);

ww_LEDR(9) <= \LEDR[9]~output_o\;

ww_LEDR(8) <= \LEDR[8]~output_o\;

ww_LEDR(7) <= \LEDR[7]~output_o\;

ww_LEDR(6) <= \LEDR[6]~output_o\;

ww_LEDR(5) <= \LEDR[5]~output_o\;

ww_LEDR(4) <= \LEDR[4]~output_o\;

ww_LEDR(3) <= \LEDR[3]~output_o\;

ww_LEDR(2) <= \LEDR[2]~output_o\;

ww_LEDR(1) <= \LEDR[1]~output_o\;

ww_LEDR(0) <= \LEDR[0]~output_o\;

ww_HEX0(7) <= \HEX0[7]~output_o\;

ww_HEX0(6) <= \HEX0[6]~output_o\;

ww_HEX0(5) <= \HEX0[5]~output_o\;

ww_HEX0(4) <= \HEX0[4]~output_o\;

ww_HEX0(3) <= \HEX0[3]~output_o\;

ww_HEX0(2) <= \HEX0[2]~output_o\;

ww_HEX0(1) <= \HEX0[1]~output_o\;

ww_HEX0(0) <= \HEX0[0]~output_o\;

ww_HEX1(7) <= \HEX1[7]~output_o\;

ww_HEX1(6) <= \HEX1[6]~output_o\;

ww_HEX1(5) <= \HEX1[5]~output_o\;

ww_HEX1(4) <= \HEX1[4]~output_o\;

ww_HEX1(3) <= \HEX1[3]~output_o\;

ww_HEX1(2) <= \HEX1[2]~output_o\;

ww_HEX1(1) <= \HEX1[1]~output_o\;

ww_HEX1(0) <= \HEX1[0]~output_o\;

ww_HEX2(7) <= \HEX2[7]~output_o\;

ww_HEX2(6) <= \HEX2[6]~output_o\;

ww_HEX2(5) <= \HEX2[5]~output_o\;

ww_HEX2(4) <= \HEX2[4]~output_o\;

ww_HEX2(3) <= \HEX2[3]~output_o\;

ww_HEX2(2) <= \HEX2[2]~output_o\;

ww_HEX2(1) <= \HEX2[1]~output_o\;

ww_HEX2(0) <= \HEX2[0]~output_o\;

ww_HEX3(7) <= \HEX3[7]~output_o\;

ww_HEX3(6) <= \HEX3[6]~output_o\;

ww_HEX3(5) <= \HEX3[5]~output_o\;

ww_HEX3(4) <= \HEX3[4]~output_o\;

ww_HEX3(3) <= \HEX3[3]~output_o\;

ww_HEX3(2) <= \HEX3[2]~output_o\;

ww_HEX3(1) <= \HEX3[1]~output_o\;

ww_HEX3(0) <= \HEX3[0]~output_o\;

ww_HEX4(7) <= \HEX4[7]~output_o\;

ww_HEX4(6) <= \HEX4[6]~output_o\;

ww_HEX4(5) <= \HEX4[5]~output_o\;

ww_HEX4(4) <= \HEX4[4]~output_o\;

ww_HEX4(3) <= \HEX4[3]~output_o\;

ww_HEX4(2) <= \HEX4[2]~output_o\;

ww_HEX4(1) <= \HEX4[1]~output_o\;

ww_HEX4(0) <= \HEX4[0]~output_o\;

ww_HEX5(7) <= \HEX5[7]~output_o\;

ww_HEX5(6) <= \HEX5[6]~output_o\;

ww_HEX5(5) <= \HEX5[5]~output_o\;

ww_HEX5(4) <= \HEX5[4]~output_o\;

ww_HEX5(3) <= \HEX5[3]~output_o\;

ww_HEX5(2) <= \HEX5[2]~output_o\;

ww_HEX5(1) <= \HEX5[1]~output_o\;

ww_HEX5(0) <= \HEX5[0]~output_o\;
END structure;


