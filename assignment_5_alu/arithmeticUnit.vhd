--------------------------------------------------------------------
--! \file      arithmeticUnit.vhd
--! \date      see top of 'Version History'
--! \brief     n-bit arithmetic unit
--! \author    Remko Welling (WLGRW) remko.welling@han.nl
--! \copyright HAN TF ELT/ESE Arnhem 
--!
--! \todo Students that submit this code have to complete their details:
--!
--! -Student 1 name         :Marvin Damman 
--! -Student 1 studentnumber: 
--! -Student 1 email address: 
--! 
--! -Student 2 name         :Joost Kuijpers 
--! -Student 2 studentnumber: 
--! -Student 2 email address: 
--!
--! -Student 2 name         :Mark Janssen 
--! -Student 2 studentnumber: 
--! -Student 2 email address: 
--!
--!
--! Version History:
--! ----------------
--!
--! Nr:    |Date:      |Author: |Remarks:
--! -------|-----------|--------|-----------------------------------
--! 001    |18-10-2019 |WLGRW   |Inital version
--! 002    |25-11-2020 |WLGRW   |Adapted for H-ESE-SOC class
--! 003    |9-12-2020  |WLGRW   |Modifications for assignment
--!
--! \todo Add revsion history when executing these assignments.
--!
--! Design:
--! -------
--! Figure 1 presents the input-output diagram of the artithmetic unit.
--! Depending on the opcode the artithmetic unit will apply the operation
--! as specified in table 1.
--! 
--!
--! \verbatim
--!
--!  Figure 1: Input-output diagram of the artithmetic unit.
--! 
--!                   +----------------+
--!               n   |                |
--!  Operand A ---/---|                |
--!                   |                |
--!               n   |                |
--!  Operand B ---/---|                |
--!                   | Arthmatic unit |   n+1
--!               3   |                |---/--- Result R
--!  Opcode F ----/---|                |
--!                   |                |
--!               4   |                |
--!  Flags P -----/---|                |
--!                   |                |
--!                   +----------------+
--!
--! \endverbatim
--!
--! Function:
--! -----------
--! Table 1: Opcodes and operations of the artithmetic unit.
--!
--! Bin | Opcode  | Functionality/Operation
--! ----|---------|--------------------------------------------------------------------------------------
--! 000 | OP_CLRR | CLR R, clear R (R:=0), all flag bits are affected
--! 001 | OP_INCA | INC A, Increment A, R:=A+1, all flag bits are affected 
--! 010 | OP_DECA | DEC A, Decrement A, R:=A-1, all flag bits are affected
--! 011 | OP_ADD  | ADD A with B, R:=A+B, all flag bits are affected
--! 100 | OP_ADC  | ADC A with B and Carry, R:=A+B+C, all flag bits are affected
--! 101 | OP_ADB  | ADB A with B and Carry, R:=A+B+C using BCD arithmetic, C and Z flag bits are affected
--! 110 | OP_SUB  | SUB B from A, R:=A-B, flag bits are affected
--! 111 | OP_SBC  | SBC B from A including C, R:=A-B-C, flag bits are affected
--! 
------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.all;  --! STD_LOGIC
USE ieee.numeric_std.all;     --! SIGNED
------------------------------------------------------------------------------
ENTITY arithmeticUnit is

   GENERIC (
      N: INTEGER := 4  --! logic unit is designed for 4-bits
      
      --! Implement here CONSTANTS as GENERIC when required.
      
   );
   
   PORT (
      A : IN  STD_LOGIC_VECTOR (N-1 DOWNTO 0); --! n-bit binary input
      B : IN  STD_LOGIC_VECTOR (N-1 DOWNTO 0); --! n-bit binary input
      P : IN  STD_LOGIC_VECTOR (3   DOWNTO 0); --! Flags input P(0)=Carry-bit
      F : IN  STD_LOGIC_VECTOR (2   DOWNTO 0); --! 3-bit opcode
      R : OUT STD_LOGIC_VECTOR (N   DOWNTO 0)  --! n+1-bit binary output
   );
   
END ENTITY arithmeticUnit;
------------------------------------------------------------------------------
ARCHITECTURE implementation OF arithmeticUnit IS
   
   -- Implement here the SIGNALS to your descretion
    SIGNAL a_sig, b_sig:      SIGNED(N DOWNTO 0);
    SIGNAL r_sig:             SIGNED(N DOWNTO 0);
    SIGNAL carry:             INTEGER RANGE 0 TO 1;

BEGIN
-- pagina 128/129 lesboek.

    a_sig <= ('0' & SIGNED(A));
    b_sig <= ('0' & SIGNED(B));
    carry <= 1 WHEN P(0)='1' ELSE 0;

   -- Implement here your arithmetic unit.
    WITH F (2 DOWNTO 0) SELECT
        r_sig <= "00000"                       WHEN "000", --OP_CLRR
                    a_sig+1                    WHEN "001", --OP_INCA
                    a_sig-1                    WHEN "010", --OP_DECA
                    a_sig+b_sig                WHEN "011", --OP_ADD
                    a_sig+b_sig+carry          WHEN "100", --OP_ADC
                    a_sig+b_sig+carry          WHEN "101", --OP_ADB BCD arithmetic???
                    a_sig-b_sig                WHEN "110", --OP_SUB
                    a_sig-b_sig-carry          WHEN "111", --OP_SBC
                    UNAFFECTED WHEN OTHERS;

 R <= STD_LOGIC_VECTOR(r_sig);

END ARCHITECTURE implementation;
